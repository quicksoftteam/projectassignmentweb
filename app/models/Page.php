<?php


namespace app\models;

use App\Core\Model;
use App\Core\H;
use App\Core\Validators\RequieValidator;

class Page extends Model
{
    public $id;
    public $content;
    public $title;
    public $date;
    public $deleted;

    public function __construct()
    {
        parent::__construct('pages');
    }
    public function validator()
	{
		try {
            if(strlen(trim(html_entity_decode($this->content))) == 0) $this->_validates;
            $this->runValidation(new RequieValidator($this, ['field' => 'title', 'msg' => 'title is required.']));
            $this->runValidation(new RequieValidator($this, ['field' => 'content', 'msg' => 'Content is required.']));
		} catch (Exception $e) {
			H::dd("page validation error: ".$e->getMessage());
		}
	}

}