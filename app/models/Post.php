<?php


namespace App\Models;


use App\Core\H;
use App\Core\Input;
use App\Core\Model;
use App\Core\Validators\RequieValidator;
use Cassandra\Date;

class Post extends Model
{
    public $id;
    public $title;
    public $content;
    public $image_feature;
    public $date;

    public $user_id;
    public $status_id;
    public $category_id;

    public function __construct()
    {
        parent::__construct('posts');
    }

    public function findDetailWithUser($postId)
    {
        return $this->query(
            "SELECT p.*, c.title as category_title, s.id as status_id, u.user_type FROM posts p 
                    inner join categories c on p.category_id = c.id 
                    inner join users u on p.user_id = u.id 
                    inner join status s on p.status_id = s.id where p.id = ?", [$postId]
                )->result();
    }

    public function findExistWithUserId($postId, $userId)
    {
        return $this->query("SELECT id FROM posts WHERE user_id = ? AND posts.id = ?", [$userId, $postId])->result();
    }

    public function findExistWithAdminAuthorize($postId)
    {
        return $this->query("SELECT id FROM posts WHERE posts.id = ?", [ $postId])->result();
    }

    public  function findAll()
    {
        return $this->query(
            "SELECT p.*, c.title as category_title, s.text as status_text FROM posts p
                inner join categories c on p.category_id = c.id
                inner join status s on p.status_id = s.id"
        )->result();
    }

    public function findByCategory($id_category, $uer_id){
    	$sql = "SELECT p.*, c.title as category_title, s.text as status_text 
				FROM 
					posts p
	                inner join categories c on p.category_id = c.id
	                inner join status s on p.status_id = s.id
	                inner join users u on p.user_id = u.id
                WHERE p.category_id = ? AND u.id = ?";
	    return $this->query($sql,[$id_category,])->result();
	}
    
    public function findWithCategory($limit)
    {
        return $this->query(
            "SELECT p.id, p.title as post_title, p.image_feature,c.id as category_id, c.title as category_title from posts p 
                inner join categories c on p.category_id = c.id  
                where p.status_id = 1  
                order by p.date desc limit $limit"
        )->result();
    }
    
    public function findWithProfile($id){
    	$sql = "SELECT p.*, c.title as category_title,p.title , p.content, p.image_feature, p.date as post_date,
				    CASE  
				        WHEN DATE_FORMAT(p.date, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d') THEN 1
				    END AS today,
				       
				    CASE 
				        WHEN DATE_FORMAT(p.date, '%Y-%m-%d') = DATE_FORMAT(subdate(now() , 1), '%Y-%m-%d')  THEN 1
				    END AS yesterday,
				       
				    CASE 
				        WHEN DATE_FORMAT(p.date, '%Y-%m-%d') <> DATE_FORMAT(subdate(now() , 1), '%Y-%m-%d') 
				                 AND  DATE_FORMAT(p.date, '%Y-%m-%d') <> DATE_FORMAT(now(), '%Y-%m-%d')  THEN 1
				    END AS other_day
				FROM posts p
				    inner join categories c on p.category_id = c.id
				    inner join status s on p.status_id = s.id 
				    inner join users u on p.user_id = u.id where u.id = ?"; 
	    return $this->query($sql,[$id])->result();
    }

    public function validator()
    {
        try {
            $this->runValidation(new RequieValidator($this, ['field' => 'title', 'msg' => 'Title can not be empty!.']));
        } catch (\Exception $e) {
        }
    }
	public function countPost(){
		return $this->query("SELECT COUNT(id) as count_id FROM posts")->result();
	}
}