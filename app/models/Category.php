<?php


namespace app\models;
use App\Core\Model;

class Category extends Model
{
    public $id;
    public $title;
    public $deleted;

    public function __construct()
    {
        parent::__construct('categories');
    }
}