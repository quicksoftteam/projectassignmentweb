<?php


namespace app\models;


use App\Core\Model;

class Settings extends Model
{
    public $id;
    public $site_title;
    public $about_us;
    public $address;
    public $email;
    public $phone1;
    public $phone2;
    public $phone3;
    public $member;

    public function __construct()
    {
        parent::__construct('settings');
    }
}