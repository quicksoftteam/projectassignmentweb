<?php

namespace App\Models;
use App\Core\H;
use App\Core\Model;
use App\Core\Validators\RequieValidator;
use Exception;

class Login extends Model
{
	public $username;
	public $email;
	public $password;
	public $remember_me;

	public function __construct()
	{
		parent::__construct('tmp_fake');
	}

	public function validator()
	{
		try {
			$this->runValidation(new RequieValidator($this, ['field' => 'email', 'msg' => 'email is required.']));
			$this->runValidation(new RequieValidator($this, ['field' => 'password', 'msg' => 'Password is required.']));
		} catch (Exception $e) {
			H::dd("Login validation error: ".$e->getMessage());
		}
	}

	public function getRememberMeChecked()
	{
		return $this->remember_me == "on";
	}}