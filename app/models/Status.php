<?php


namespace app\models;


use App\Core\Model;

class Status extends Model
{
    public $id;
    public $text;
    public $date;

    public function __construct()
    {
        parent::__construct('status');
    }
}