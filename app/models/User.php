<?php

namespace App\Models;
use App\Core\FH;
use App\Core\H;
use App\Core\Model;
use App\Core\Session;
use App\Core\Cookie;
use App\Core\Validators\{MinValidator, MaxValidator, UniqueValidator};

class User extends Model
{
	private $_sessionName;
	private $_cookieName;
	private $_userObject = 'App\Models\User';
	private $_confirm;
	public static $currentLoggedInUser = null;
	
	public $id;
	public $username;
	public $email;
	public $password;
	public $fname;
	public $lname;
	public $acl;
	public $deleted = 0;
	public $phone;
	public $created_date;
	public $photo;
	public $user_type;
	

	public function __construct($user = '')
	{
		parent::__construct('users');
		$this->_sessionName = CURRENT_USER_SESSION_NAME;
		$this->_cookieName = REMEMBER_ME_COOKIE_NAME;
		$this->_softDelete = true;
		if (!empty($user))
        {
            if (is_int($user))
            {
                $u = $this->_db->findFirst($this->_table, ['conditions' => 'id = ?', 'bind' => [$user]], $this->_userObject);
            }
            else
            {
                $u = $this->_db->findFirst($this->_table, ['conditions' => 'username = ?', 'bind' => [$user]], $this->_userObject);
            }
            if ($u)
            {
                foreach ($u as $key => $value)
                {
                    $this->$key = $value;
                }
            }
        }
	}

	public static function loginUserFromCookie()
	{
		$user_session = UserSessions::getFromCookie();
		$user = null;
		if ($user_session && !empty($user_session->user_id))
		{
			$user = new self((int)$user_session->user_id);
			if ($user)
			{
				$user->login();
			}
			return $user;
		}
		return false;
	}

	public function findByUsername($username)
	{
		return $this->_db->findFirst($this->_table, ['conditions' => 'username = ?', 'bind' => [$username]],$this->_userObject);
	}

	public function login($rememberMe = false)
	{
		Session::set($this->_sessionName, $this->id);
		if ($rememberMe)
		{
			$hash =  md5(uniqid() . rand(0, 500));
			$user_agent = Session::uagentVersion();
			Cookie::set($this->_cookieName, $hash, REMEMBER_ME_COOKIE_EXPIRE);
			$fields = ['sessions' => $hash, 'user_agent'=>$user_agent, 'user_id'=>$this->id];
			$this->_db->query('DELETE FROM user_sessions WHERE user_id = ? AND user_agent = ? ', [$this->id, $user_agent]);
			$this->_db->insert('user_sessions', $fields);
		}
		Session::set('is_loggedIn', true);
	}

	public static function currentLoggedInUser()
	{
		if (!isset(self::$currentLoggedInUser) && Session::exist(CURRENT_USER_SESSION_NAME))
		{
			self::$currentLoggedInUser = new User((int)Session::get(CURRENT_USER_SESSION_NAME));
		}
		return self::$currentLoggedInUser;
	}

	public function logout()
	{
        Session::set('is_loggedIn', false);
		$user_session = UserSessions::getFromCookie();
		if ($user_session) $user_session->delete();
		Session::delete(CURRENT_USER_SESSION_NAME);
		if (Cookie::exist(REMEMBER_ME_COOKIE_NAME))
		{
			Cookie::delete(REMEMBER_ME_COOKIE_NAME);
		}
		self::$currentLoggedInUser = null;
		return true;
	}

	public function acls()
	{
		if (empty($this->acl)) return [];
		return json_decode($this->acl, true);
	}

	public function validator()
	{
        try {
			// $this->runValidation(new MinValidator($this, ['field' => 'password', 'rule' => 8, 'msg' => 'Username must be a minimum of 8 characters.']));
			// $this->runValidation(new MaxValidator($this, ['field' => 'password', 'rule' => 20, 'msg' => 'Username must be a maximum of 20 characters.']));
			// $this->runValidation(new UniqueValidator($this, ['field' => 'email', 'msg' => 'This email are already exists.']));
			
        } catch (\Exception $e) {
            H::dd($e->getMessage());
        }
    }

	public function setConfirm($value)
	{
		$this->_confirm = $value;
	}

	public function getConfirm()
	{
		return $this->_confirm;
	}

	public function beforeSave()
	{
		if ($this->isNew())
		{
			$this->password = password_hash($this->password, PASSWORD_DEFAULT);
		}
	}

	public function blackList(){
		$arr = ['password', 'acl', 'created_date', 'role_id'];
		return $arr;
	}

	public function getAll(){
		return $this->query('SELECT u.* ,t.type  FROM users u LEFT JOIN user_type t ON u.user_type = t.id');
	}

	public function findUser($id){
		return $this->query('SELECT u.* ,t.type  FROM users u LEFT JOIN user_type t ON u.user_type = t.id WHERE u.id='.$id);
		// return $this->findById($id);
	}

	public function getAllUerType(){
		return $this->query('SELECT * FROM user_type');
	}

	public function getLastId(){
		return $this->query("SELECT LAST_INSERT_ID() as id");
	}

	public function saveRole()
	{
		if ($this->id)
		{
			$role_model = new Roles();
			$role = $role_model->findUserRoles($this->id)->result();
			$role = count($role)? $role[0] : null;
			return Session::set('UserRole', objectToArray($role));
		}
		return false;
	}	

	public function getTypeUser($type){
	    return $this->query("SELECT type FROM user_type where id = ?",[$type]);
    }

    public function getCategory($id){
	    $sql = "SELECT 
						p.category_id,c.title ,count(*) as 'count_of_curs'
					FROM 
						posts p 
						inner join categories c on p.category_id = c.id
						inner join users u on p.user_id = u.id
					where u.id = ? group by category_id";
	    return $this->query($sql,[$id])->result();
    }
    
    public function countUser(){
	    return $this->query("SELECT COUNT(id) as count_id FROM users")->result();
    }
} 