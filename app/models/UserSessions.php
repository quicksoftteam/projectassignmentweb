<?php

namespace App\Models;
use App\Core\Cookie;
use App\Core\Model;
use App\Core\Session;

class UserSessions extends Model
{
	private $_userSessionObject = 'UserSessions';
	public $id;
	public $user_id;
	public $session;
	public $user_agent;
	
	public function __construct()
	{
		$table = 'user_sessions';
		parent::__construct($table);
	}

	public static function getFromCookie()
	{
		$user_session = new self();
		$user = $user_session->findFirst([
			'conditions' => "user_agent = ? AND sessions = ?",
			'bind' => [Session::uagentVersion(), Cookie::get(REMEMBER_ME_COOKIE_NAME)]
		]);
		if (!$user) return false;
		return $user;
	}
}