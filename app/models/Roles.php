<?php
namespace app\models;
use App\Core\FH;
use App\Core\Model;
use ReflectionClass;

class Roles extends Model
{
    public $id;
    public $user_id;

    public $edit_post;
    public $denied_post;
    public $create_post;
    public $approve_post;
    public $delete_post;
    public $waiting_post;
    public $view_all_post;

    public $view_user;
    public $create_user;
    public $update_user;
    public $disable_user;

    public $create_status;
    public $update_user_role;
    public $created_date_roles;

    public $edit_menu;
    public $view_profile;

    public $create_category;
    public $edit_category;
    public $delete_category;
    public $view_all_category;

    public function __construct()
    {
        parent::__construct('roles');
    }

    public function findUserRoles($id){
		return $this->query('SELECT * FROM roles WHERE user_id = ? limit 1',[$id]);
    }

    public function getDefualtObject(){
        return FH::getObjectProperties($this);
    }
}