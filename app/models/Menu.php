<?php


namespace app\models;


use App\Core\Model;

class Menu extends Model
{
    public $id;
    public $name;
    public $parent_id;
    public $link;
    public $status;

    public function __construct()
    {
        parent::__construct('menu');
    }

}