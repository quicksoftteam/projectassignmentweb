<?php


namespace app\controllers;


use App\Core\Controller;
use App\Core\H;
use App\Core\Router;
use App\Models\Post;

class DynamicPostController extends Controller
{
    private $_category = '';
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->_category = $action;
    }

    public function indexAction()
    {
        $page_number = $this->request->get('p');
        if (intval($page_number))
        {
            //@TODO pagination
        }
        $list_post = '';
        $list_title = 'List Post';
        if (strcmp($this->_category, 'index') == 0) Router::redirect('restricted');
        $this->_category = H::decodeUrl($this->_category);
        $post_model = new Post();
        $list_post = $post_model
            ->query(
                'select p.* , c.title as category_title, u.username as username
                    from posts p 
                    inner join categories c on p.category_id = c.id
                    inner join users u on p.user_id = u.id
                    where  p.category_id = c.id and c.title = ?',
                [$this->_category])
            ->result();
        if (!$list_post)
        {
            Router::redirect('restricted');
        }

        $this->view->list = $list_post;
        $this->view->listTitle = $list_title;
        $this->view->render('posts/list');
    }

    public function detailAction($id = 0, $category = '')
    {
        $post_model = new Post();
        $category = H::decodeUrl($category); // to serialize data from url to database
        $post =  $post_model
            ->query(
                'select p.* , c.title as category_title , u.username as username 
                    from posts p 
                    inner join categories c on p.category_id = c.id
                    inner join users u on p.user_id = u.id
                    where  (p.category_id = c.id and c.title = ?) and p.id = ?  limit 1', // status approve = 1
                [$category, $id]) // didn't use $this->_category due to it did work properly and category pass from router url
            ->result();
        if (!$post)
        {
            Router::redirect('restricted');
        }
        $this->view->post = $post[0];
        $this->view->render('posts/view');
    }
}