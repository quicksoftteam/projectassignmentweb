<?php

namespace App\Controllers;
use App\Core\Controller;
use App\Core\FH;
use App\Core\Router;
use App\Models\Login;
use App\Models\User;

class RegisterController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->loadModel('User'); // to generate UsersModel
	}

	public function loginAction()
	{
		$login_model = new Login();

		if ($this->request->isPost())
		{
			$this->request->csrfCheck();
			$login_model->assign($this->request->get());
			$login_model->validator();
			if ($login_model->validationPassed())
			{
				$user = $this->UserModel->findFirst([
					"conditions" => "username = ? or email = ?",
					"bind" =>[$_POST['email'], $_POST['email']],
					"limit" => 1
				], "App\Models\User"); // use index 0 as object because find function return array object
				if ( $user && password_verify($this->request->get('password'), $user->password))
				{
					$remember_me = $login_model->getRememberMeChecked();
					$user->login($remember_me); // this function is in the model class
                    $user->saveRole();
                    if (in_array($user->user_type, [1,2]))
					    Router::redirect('admin/dashboard');
                    else
					    Router::redirect('');
				}
				else
				{
					$login_model->addErrorMessage('username', "There is an error with username or password.");
				}
			}
		}
		$this->view->login = $login_model;
		$this->view->displayErrors = $login_model->getErrorMessages();
		$this->view->render('register/login');
	}
	
	public function signupAction()
	{
		$new_user = new User();
		if ($this->request->isPost()) {
			$this->request->csrfCheck();
			$new_user->assign($this->request->get());
			$new_user->setConfirm($this->request->get('confirm-password'));
			if ($new_user->save())
			{
				Router::redirect('');
			}
		}
		$this->view->newUser = $new_user;

		$this->view->displayErrors =$new_user->getErrorMessages();
		$this->view->render('register/signup');
	}

	public function logoutAction()
	{
		if (FH::currentUser())
		{
			FH::currentUser()->logout();
		}
		Router::redirect('register/login');
	}
}