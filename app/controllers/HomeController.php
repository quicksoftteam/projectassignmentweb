<?php

namespace App\Controllers;
use App\Core\Controller;
use App\Core\Session;
use App\Models\Post;
use app\models\Settings;
use stdClass;

class HomeController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
	}

	public function indexAction()
    {
        $setting_model = new Settings();
        $setting = $setting_model->findFirst();
        $post_model = new Post();
        $posts = $post_model->findWithCategory(100);
        $category_count = [];
        foreach ($posts ?? [] as $post)
        {
            if (!array_key_exists($post->category_id, $category_count))
            {
                $category_count[$post->category_id] = new stdClass();
                $category_count[$post->category_id]->id = $post->category_id;
                $category_count[$post->category_id]->title = $post->category_title;
            }
        }
        $this->view->categories = $category_count;
        $this->view->setting = Session::set('settings', objectToArray($setting ?? $setting_model));
        $this->view->posts = Session::set('posts', objectToArray($posts ?? $post_model));
		$this->view->render('home/index');
	}
}