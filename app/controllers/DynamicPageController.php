<?php


namespace app\controllers;


use App\Core\Controller;
use App\Core\H;
use App\Core\Router;

class DynamicPageController extends Controller
{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->loadModel('Page');
    }

    public function indexAction($title = '')
    {
        $page = $this->PageModel->findFirst([
           'conditions' => ['title'=>'?'],
           'bind' => [$title]
        ]);
        if (!$page)
        {
            Router::redirect('restricted');
        }
        $this->view->title = $page->title;
        $this->view->body = $page->content;
        $this->view->render('page');
    }
}