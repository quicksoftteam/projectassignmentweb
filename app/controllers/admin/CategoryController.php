<?php

namespace App\Controllers\Admin;
use App\Core\Controller;
use App\Core\Router;
use app\models\Category;
use App\Models\Post;

class CategoryController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->view->setLayout('admin/layouts/default');

	}

	public function indexAction()
	{
	    $cate_model = new Category();
        $categories = $cate_model->find();
	    $this->view->list = !$categories? [] : $categories;
		$this->view->render("admin/category");
	}
	
	public function saveAction()
    {
        if ($this->request->isPost())
        {
            $cat_model = new Category();
            $cat = $cat_model->find([
                'conditions' => ['title'=>'?'],
                'bind' => [$this->request->get('title')]
            ]);
            if (!$cat)
            {
                $req = $this->request->get();
                $cat_model->assign($req);
                $cat_model->deleted = 0;
                if ($cat_model->save())
                {
                    $cat_list = $cat_model->find();
                    $this->responseJson(200, true, $cat_list, 'Added succeed !');
                }
                else
                {
                    $this->responseJson(200, false, $cat, 'Error while add category !');
                }
            }
            else
            {
                $this->responseJson(200, false, $cat, 'Category are already exist!');
            }
        }
        $this->view->render('admin/category');
    }
	
	public function updateAction()
    {
        if ($this->request->isPost())
        {
            $category_model = new Category();
            $category_model->assign($this->request->get());
            if (!isset($category_model->deleted))
            {
                $this->responseJson(200, false, [], "Category's Status can't be null !");
            }
            else
            {
                if ($category_model->save())
                {
                    $this->responseJson(200, true, [], "Category updated!");
                }
                $this->responseJson(200, false, [], "Category failed save!");
            }
        }
        $this->view->render('admin/category');
    }
    
    public function deleteAction($id = 0)
    {
        if ($this->request->isPost())
        {
            $post_model = new Post();
            $post = $post_model->find([
                'conditions'=> 'category_id = ?',
                'bind' => [$id]
            ]);
            if ($post)
            {
                $this->responseJson(200, false, [], "Can't delete This category are in used!");
            }
            else
            {
                $category_model = new Category();
                if ($category_model->delete($id))
                {
                    $this->responseJson(200, true, [], 'Category deleted!');
                }
                $this->responseJson(200, false, [], 'Category failed to deleted');
            }
        }
        $this->view->render('admin/category');
    }
}