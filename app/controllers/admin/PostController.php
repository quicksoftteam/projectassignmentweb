<?php

namespace App\Controllers\Admin;
use App\Core\Controller;
use App\Core\FH;
use app\core\File;
use App\Core\H;
use App\Core\Router;
use App\Core\Session;
use app\core\StatusCode;
use app\models\Category;
use App\Models\Post;
use app\models\Status;

class PostController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->view->setLayout('admin/layouts/default');

		// load model to easy to access to database
        $this->loadModel('Post');
	}

	public function indexAction()
	{
	    $post_model = new Post();
	    $post_list = $post_model->findAll();

	    $this->view->posts = $post_list ?? [];
		$this->view->render("admin/post/index");
	}
	
	public function findAction($id_category){
		$post_model = new Post();
		$user = FH::currentUser();
		$post_list = $post_model->findByCategory($id_category,$user->id);
		$this->view->posts = $post_list ?? [];
		$this->view->render("admin/post/index");
	}

    public function addAction()
    {
        $category_model  = new Category();
        $post_model = new Post();
        $categories = $category_model->find();
        if ($this->request->isPost())
        {
            $post_model->assign($this->request->get());
            $user = FH::currentUser();
            $post_model->user_id = $user->id;
            $post_model->status_id = 2; // waiting
            if ($post_model->validationPassed())
            {
                if ($image_feature = File::uploadImage('image_feature'))
                {
                    $post_model->image_feature = $image_feature;
                }
                if ($post_model->save())
                {
                    H::addMsgToast('success', 'added post success!');
                }
                else
                {
                    H::addMsgToast('error', $post_model->getErrorMessages());
                }
            }
        }
        $this->view->post = $post_model;
        $this->view->categories = $categories ?? [];
        $this->view->render("admin/post/add");
    }

    public function updateAction($id = '')
    {
        if (intval($id))
        {
            $category_model  = new Category();
            $post_model = new Post();
            $status_model = new Status();
            $status = $status_model->find();
            $categories = $category_model->find();
            $user = FH::currentUser();
            $post = $post_model->findDetailWithUser($id, $user->id);
            if (!$post) Router::redirect('restricted');
            if ($this->request->isPost())
            {
                $post_model->assign($post[0]);
                $post_model->assign($this->request->get());
                if ($image_feature = File::uploadImage('image_feature'))
                {
                    File::deleteUploadImage($post[0]->image_feature);
                    $post_model->image_feature = $image_feature;
                    $post[0]->image_feature = $image_feature;
                }
                if ($post_model->save())
                    H::addMsgToast('success', 'Post updated!');
                else
                    H::addMsgToast('error', 'Post failed to update.');
                Router::redirect('admin/post');
            }
            $this->view->categories = $categories;
            $this->view->post = count($post) > 0 ? $post[0]  : $post_model ;
            $this->view->status = $status ?? $post_model ;
            $this->view->render("admin/post/edit");
        }
        else Router::redirect('restricted');
    }

    public function deleteAction($id = '')
    {
        $this->setStatus($id, 4);
    }

    public function approveAction($id = '')
    {
        $this->setStatus($id, 1, false);
    }

    public function waitingAction($id = '')
    {
        $this->setStatus($id, 2, false);
    }

    public function deniedAction($id = '')
    {
        $this->setStatus($id, 3, false);
    }

    public function setStatus($id, $status, $returnJson = true)
    {
        if ($this->request->isPost() OR !$returnJson) //return json it mean that change status
        {
            $message_success = [
                1 => 'Post approved',
                2 => 'Post waiting',
                3 => 'Post denied',
                4 => 'Post deleted'
            ];
            $message_failed = [
                1 => 'Post failed to approved',
                2 => 'Post failed to waiting',
                3 => 'Post failed to denied',
                4 => 'Post failed to deleted'
            ];
            $user = FH::currentUser();
            $role = Session::get('UserRole');
            $post_model = new Post();
            if ($user->user_type != SUPER_ADMIN_TYPE){
                if (
                    ($role['approve_post'] == 1 && $status == 1)
                    OR
                    ($role['waiting_post'] == 1 && $status == 2)
                    OR
                    ($role['denied_post'] == 1 && $status == 3)
                    OR
                    ($role['delete_post'] == 1 && $status == 4)
                )
                    $post = $post_model->findExistWithAdminAuthorize($id);
                else
                    $post = false;
            }
            else
                $post = $post_model->findExistWithAdminAuthorize($id);
            if ($post)
            {
                $post_model->update($id, ['status_id'=> $status]);
                if ($returnJson)
                    $this->responseJson(StatusCode::$success, true, $id, $message_success[$status]);
                else
                    H::addMsgToast('success', $message_success[$status]);
            }
            else
            {
                if ($returnJson)
                    $this->responseJson(StatusCode::$success, false, $id, $message_failed[$status]);
                else
                    H::addMsgToast('error', $message_failed[$status]);
            }
            Router::redirect('admin/post');
        }
    }
}