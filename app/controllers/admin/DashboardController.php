<?php

namespace App\Controllers\Admin;
use App\Core\Controller;
use App\Core\H;
use App\Models\Post;
use App\Models\User;

class DashboardController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->view->setLayout('admin/layouts/default');
	}

	public function indexAction()
	{
		$post_model = new Post();
		$user = new User();
		$post_list = $post_model->findAll();
		$this->view->post = $post_model->countPost()[0];
		$this->view->user = $user->countUser()[0];
		$this->view->posts = $post_list ?? [];
		$this->view->render("admin/dashboard");
	}

}