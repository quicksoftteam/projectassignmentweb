<?php

namespace App\Controllers\Admin;
use App\Core\Controller;
use App\Core\H;
use App\Core\Session;
use App\Models\Post;
use App\models\User;
use App\models\Roles;
use App\Core\FH;
use App\Core\Router;

class UserController extends Controller
{
	public function __construct($controller, $action)
	{
		
		parent::__construct($controller, $action);
	     $this->view->setLayout('admin/layouts/default');
	}

	public function indexAction()
	{
		$u = new User();
		$this->view->users = $u->getAll()->result();
		$this->view->render("admin/user/user");
	}

	public function disableAction($id,$st)
	{
		$u = new User();
		if($st == 0){
			$stu = 1;
		}
		else
		{
			$stu = 0;
		}
		 $u->update($id, ['deleted'=>$stu]);
		 Router::redirect('admin/user/index');
	}

	public function upLoadAction()
	{
		if(isset($_POST["image"]))
		{
			$data = $_POST["image"];
			$image_array_1 = explode(";", $data);
			$image_array_2 = explode(",", $image_array_1[1]);
			$data = base64_decode($image_array_2[1]);
			$imageName = time() .'.png';
			file_put_contents(PATH_IMAGE_USERS_UPLOAD.$imageName, $data);
			Session::set('photo', $imageName);
			echo H::resolveImageUrl($imageName);
		}
	}

	public function addAction()
	{
		$u = new User();
		$r = new Roles();
		if($this->request->isPost()){
			$para = $this->request->get();
			$para['photo'] = Session::get('photo');
			Session::delete('photo');
			$u->assign($para);
			if ($u->save())
			{
				$lastId = $u->getLastId()->result()[0];
				$para['user_id'] = $lastId->id;
				$r->assign($para);
				if($r->save())
				{
					Router::redirect('admin/user/index');
				}
			}
		}
		$currentUser = FH::currentUser();
		$this->view->users = $u;
		$this->view->currentUser = $currentUser;
		$this->view->users_Roles =$r->getDefualtObject();
		$this->view->user_type = (array)$u->getAllUerType()->result();
		$this->view->displayErrors = $u->getErrorMessages();
		$this->view->render("admin/user/addUser");
	}

	public function updateAction(){
		$u = new User();
		$r = new Roles();
		if($this->request->isPost()){
			$para = $this->request->get();
			if(Session::get('photo'))
			{
				$para['photo'] = Session::get('photo');
				Session::delete('photo');
			}
			else
			{
				$para['photo'] = $_POST['photo_pro'];
			}
			$r->assign($para, ['id']);
			$u->assign($para);
			$us =  $u->assign_field($para);
			$us_id = $us['id'];
			if ($u->update($us_id, $us))
			{
				$ur =  $r->assign_field($para);
				$ur['id'] = $this->request->get('id_role');
				if(!empty($ur['id']))
				{
					$ur['user_id'] = $us_id;
				    $r->assign($ur);
					if($r->save())
					{
						Router::redirect('admin/user/index');
					}
				}
			}
		}
		// when it error
		// Router::redirect('admin/user/edit/'.$u->id);
	}


	public function editAction($id)
	{
		$u = new User();
		$r = new Roles();
		$ur = $r->findUserRoles($id)->result()[0];
		$this->view->user_type = (array)$u->getAllUerType()->result();
		$this->view->users = $u->findUser($id)->result()[0];
//		echo "<pre>";
//		var_dump($ur);
//		exit();
		$this->view->users_Roles = $ur ?? $r->getDefualtObject();
		$this->view->render("admin/user/editUser");
	}

	public function chagepwAction()
	{
		if($this->request->isPost()){
			$u = new User();
			$us = $this->request->get();
			$new_pw = password_hash ($us["password"], PASSWORD_DEFAULT);
			if($us["password"] == $us["comf_password"]){
				if($u->update($us["id_user"], ["password" => trim($new_pw)])){
					$this->responseJson(200, true, [] , "Password changed!<br>This is your password : ".$us["password"]);
				}
				$this->responseJson(200, false, [], "Password failed to chang!");
			}
			else
			{
				$this->responseJson(200, false, [], "Comfirm Password Not Correct!");
			}
		}
	}
	
	public function profileAction()
	{
		$post_model = new Post();
		$u = new User();
		$user = FH::currentUser();
		$user->user_type = $u->getTypeUser($user->user_type)->result()[0]->type;
		$cog =  $u->getCategory($user->id);
        $this->view->user_profile = $user;
        $this->view->count_category = $cog;
		$post_list = $post_model->findWithProfile($user->id);
		$this->view->post_list = $post_list ?? $post_model;
		$this->view->render("admin/user/profile");
	}

}