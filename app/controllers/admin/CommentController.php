<?php

namespace App\Controllers\Admin;
use App\Core\Controller;

class CommentController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->view->setLayout('admin/layouts/default');
	}

	public function indexAction()
	{
		$this->view->render("admin/comment");
	}

}