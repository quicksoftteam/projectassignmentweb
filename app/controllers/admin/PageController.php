<?php

namespace App\Controllers\Admin;
use App\Core\Controller;
use App\Core\H;
use App\Core\FH;
use App\Models\Page;

class PageController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
        $this->view->setLayout('admin/layouts/default');
        $this->loadModel('Page');
	}

	public function indexAction()
	{
        $page_model = new Page();
	    $page_list = $page_model
            ->query("SELECT * FROM pages")
            ->result();
	    $this->view->pages = $page_list ?? [];
		$this->view->render("admin/page/index");
    }
    
    public function addAction()
    {
        $page_model = new Page();
        if ($this->request->isPost())
        {
            $page_model->assign($this->request->get());
            $page_model->validator();
            if ($page_model->validationPassed())
            {
                if ($page_model->save())
                {
                    H::addMsgToast('success', 'success added page');
                }
                else
                {
                    H::addMsgToast('error', 'added fail');
                }
            }
            else
                H::addMsgToast('error','added fail');
        }
        $this->view->page = $page_model;
        $this->view->render("admin/page/add");
    }

    public function updateAction($id=0)
    {
        if (intval($id) && $id !=0 && !$this->request->isPost())
        {
             $page_model = new Page();
             $page=$page_model->query("SELECT * FROM pages p where p.id=?",[$id])->result();
        }
        // in case click button update
         if($this->request->isPost())
         {
            $page_model = new Page();
            $page_model->assign($this->request->get());
            if($page_model->save())
            {
                $this->responseJson(200, true, [], "Update Successful!");
            }
        }
        $this->view->page = $page[0] ?? $page_model;
        $this->view->id = $id;
        $this->view->render("admin/page/edit");
    }

    public function deleteAction($id)
    {
        if ($this->request->isPost())
        {
            $page = $this->request->get();

            $page_model = new Page();
            
            if ($page_model->delete($id))
            {
                $this->responseJson(200, true, [], 'success deleted page');
            }
            $this->responseJson(200, false, [], 'deleted page failed');
        }
    }
}

