<?php

namespace App\Controllers;
use App\Core\Controller;

/**
 * ContactUs Controller
 */
class ContactUsController extends Controller
{
	
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
	}
}