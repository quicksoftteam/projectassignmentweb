<?php
use App\Core\FH;
use App\Core\H;

$this->setSiteTitle('Category');
?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
    <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
    <div>
    </div>
    <a href="" class="white-text mx-3 h4 font-weight-bold">ប្រភេទនៃអត្ថបទ</a>
    <div>
    </div>

</div>
<?php if (FH::isAuthorize('create_category')):?>
<div class="row d-flex justify-content-center">
    <button type="button" class="btn btn-outline-success btn-rounded waves-effect" data-toggle="modal" data-target=".formAdd">បន្ថែមប្រភេទការប្រកាស</button>
</div>
<!-- modal form add  -->
<div class="modal fade formAdd" tabindex="-1" role="dialog" aria-labelledby="formAdd" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- Material form contact -->
            <div class="card">
                <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                    <div>
                        <a class="white-text mx-3">បន្ថែមការប្រកាស</a>
                    </div>
                </div>
                <!--Card content-->
                <div class="card-body px-lg-5 pt-0">
                    <!-- Form -->
                    <form style="color: #757575;" id="formAdd" class="text-center needs-validation" novalidate>
                        <!-- Category -->
                        <div class="md-form mt-3">
                            <input type="text" id="title" name="title" class="form-control" required autofocus>
                            <label for="name">ចំណងជើង</label>
                            <div class="invalid-feedback">
                               បញ្ចូលហ្វាល់
                            </div>
                        </div>
                        <!-- Send button -->
                        <button id="btnAdd" class="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit">បន្ថែម</button>

                    </form>
                    <!-- Form -->
                </div>
            </div>
            <!-- Material form contact -->
        </div>
    </div>
</div>
<?php endif;?>

<?php if (FH::isAuthorize('edit_category')):?>
<!-- modal form update  -->
<div class="modal fade formUpdate" tabindex="-1" role="dialog" aria-labelledby="formUpdate" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- Material form contact -->
            <div class="card">
                <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                    <a href="#" class="white-text mx-3 text-center">កែប្រែប្រភេទការប្រកាស</a>
                </div>

                <!--Card content-->
                <div class="card-body px-lg-4 pt-0">
                    <!-- Form -->
                    <form class="text-center" id="formUpdate" style="color: #757575;">
                        <input type="hidden" name="id">
                        <!-- Category -->
                        <div class="md-form mt-3">
                            <input type="text" name="title" class="form-control" autofocus>
                            <label for="title">ចំណងជើង</label>
                        </div>
                        <!-- STATUS -->
                        <!-- true-->
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="status_true" name="deleted" value="0">
                            <label class="form-check-label" for="status_true">Active</label>
                        </div>

                        <!-- false-->
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="status_false" name="deleted" value="1">
                            <label class="form-check-label" for="status_false">Deactivated</label>
                        </div>
                        <!-- Send button -->
                        <button class="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit" id="btnConfirmUpdate">Update</button>

                    </form>
                    <!-- Form -->
                </div>
            </div>
            <!-- Material form contact -->
        </div>
    </div>
</div>
<?php endif;?>

<?php if (FH::isAuthorize('delete_category')):?>
<!-- Modal form delete-->
<div class="modal fade formDelete" tabindex="-1" role="dialog" aria-labelledby="formDelete"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" id="formDelete">
                <div class="modal-header">
                    <h5 class="modal-title" >លុបប្រភេទនៃការប្រកាស: <label name="title" class="title">s</label></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   តើអ្នកប្រាកដចិត្តទេថានឹងលុបប្រភេទនៃការប្រកាសមួយនេះ?
                </div>
                    <input type="hidden" name="id">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="btnConfirmDelete">លុប</button>
                    </div>
                </div>
        </form>
    </div>
</div>
<?php endif;?>
<div class="px-4">

    <table id="tableCategory" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">លេខសម្គាល់    
                </th>
                <th class="th-sm">ប្រភេទ
                </th>
                <th class="th-sm">STATUS
                </th>
                <?php if (FH::isAuthorize(['edit_category', 'delete_category'])):?>
                <th class="th-sm">
                    សកម្មភាព
                </th>
                <?php endif;?>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($this->list as $category):?>
            <tr>
                <td class="id"><?=$category->id?></td>
                <td class="title"><?=$category->title?></td>
                <td><input type="hidden" name="status" value="<?=$category->deleted?>"><?=$category->deleted? 'Deactivated' : 'Active'?></td>
                <?php if (FH::isAuthorize(['edit_category', 'delete_category'])):?>
                <td  class="text-center center_td ">

                    <?php if (!$category->deleted):?>
                        <a href="<?=H::resolvePostListUrl($category->title)?>" class="btn-floating btn-sm waves-effect text-center" title="ចុចមើល"><i class="far fa-eye text-info"></i></a>
                    <?php endif;?>
                    <?php if (FH::isAuthorize('edit_category')):?>
                    <a class="btn-floating btn-sm waves-effect text-center btnUpdate" data-toggle="modal" data-target=".formUpdate" title="កែប្រែ"><i class="far fa-edit text-success"></i></a>
                    <?php endif;?>

                    <?php if (FH::isAuthorize('delete_category')):?>
                    <a class="btn-floating btn-sm waves-effect btn-sm btnDelete" data-toggle="modal" data-target=".formDelete" title="លុប"><i class="fa fa-trash-alt text-danger"></i></a>
                    <?php endif;?>
                </td>
                <?php endif;?>
            </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <th class="th-sm">លេខសម្គាល់
                </th>
                <th class="th-sm">ប្រភេទ
                </th>
                <th class="th-sm">STATUS
                </th>
                <?php if (FH::isAuthorize(['edit_category', 'delete_category'])):?>
                <th>សកម្មភាព
                </th>
                <?php endif;?>
            </tr>
        </tfoot>
    </table>

</div>
<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script>
$(document).ready(function() {
    $('#tableCategory').DataTable({
        "language":{
            "lengthMenu": "បង្ហាញ _MENU_ ចំនួនជួរដេក",
            "zeroRecords": "មិនមានទិន្នន័យ",
            "info": "បង្ហាញទំព័រ _PAGE_ នៃ _PAGES_",
            "infoEmpty": "មិនមានទិន្នន័យ",
            "infoFiltered": "(យកចេញ ពី _MAX_ នៃទិន្នន័យសរុប)",
            "search":"ស្វែងរក",
            "paginate":{
                "next":"បន្ទាប់",
                "previous":"ត្រលប់",
                "first":"តំបូង",
                "last":"ចុងក្រោយ",
            }
        }
    });
    $('.dataTables_length').addClass('bs-select');
    <?php if (FH::isAuthorize('create_category')):?>
    $('#tableCategory').on('click','#btnAdd', function (e) {
        e.preventDefault();
        let form = $("#formAdd").serializeArray();
        if (form[1].value.trim() == ''){
            toastr.error('Please input title.');
            return false;
        }
        $.showSpinner();
        $.post('admin/category/save', form, function (result) {
            $.hideSpinner();
            if (result.success) {
                toastr.success(result.message);
                $('.formAdd').modal('hide');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            } else {
                toastr.error(result.message ?? 'internal error.');
            }
            
        });
    });
    <?php endif;?>

    <?php if (FH::isAuthorize('edit_category')):?>
    $('#tableCategory').on('click','.btnUpdate', function (e) {
        e.preventDefault();
        let id = $(this).parent().parent().find('.id').text();
        let text = $(this).parent().parent().find('.title').text();
        let deleted = $(this).parent().parent().find('input[name="status"]').val();
        let form = $('#formUpdate');
        form.children('input[name="id"]').val(id);
        form.find('input[name="title"]').focus().val(text);
        form.find('input[name="deleted"][value=' + deleted + ']').prop('checked', true);
    });

    $('#btnConfirmUpdate').on('click', function (e) {
        e.preventDefault();
        let form = $("#formUpdate").serializeArray();
        if (form[1].value.trim() == ''){
            toastr.error('Please input title.');
            return false;
        }
        $.showSpinner();
        $.post('admin/category/update', form, function (result) {
            $.hideSpinner();
            if (result.success) {
                $('.formUpdate').modal('hide');
                toastr.success(result.message);
                setTimeout(function () {
                    location.reload();
                }, 1000);
            } else {
                toastr.error(result.message);
            }

        });
    });
    <?php endif;?>

    <?php if (FH::isAuthorize('delete_category')):?>
    $('#tableCategory').on('click','.btnDelete', function () {
        let id = $(this).parent().parent().find('.id').text();
        let text = $(this).parent().parent().find('.title').text();
        let form = $('#formDelete');
        form.children('input[name="id"]').val(id);
        form.find('label[name="title"]').html(text);
    });

    $('#btnConfirmDelete').on('click', function (e) {
        e.preventDefault();
        let form = $("#formDelete");
        let  id = form.children('input[name="id"]').val();
        $.showSpinner();
        $.post('admin/category/delete/' + id, form, function (result) {
            $.hideSpinner();
            if (result.success) {
                toastr.warning(result.message);
                setTimeout(function () {
                    location.reload();
                }, 1000);
                $('.formDelete').modal('hide');
            } else {
                toastr.error(result.message ?? 'internal error.');
            }

        });
    });
    <?php endif;?>
});
</script>
<?php $this->end(); ?>