<?php $this->setSiteTitle('Category');

use App\Core\FH;
use App\Core\H; ?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
<div
    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center000000000000">

    <div>
    </div>

    <a href="" class="white-text mx-3 font-weight-bold h4">ផ្ទាំងព័ត៌មាន</a>

    <div>
    </div>

</div>
<div class="row text-center">

    <div class="col-md-3">
		    <div class="card" style="width: 200px; height: 200px">
			    <div class="view " style="width: 100px; height: 200px">
				    <img style=" height: 80px" src="<?=PROJECT_PATH?>public/img/admin/managemen.jpg"
				         alt="Card image cap">
				    <a>
					    <div class="mask rgba-white-slight"></div>
				    </a>
			    </div>
			    <!-- Card content -->
			    <div class="card-body">
				    <!-- Title -->
				    <h6 class="card-title">អ្នកគ្រប់គ្រង</h6>
				    <hr>
				    <!-- Text -->
				    <p class="card-text text-center"><?=$this->user->count_id?> នាក់</p>

			    </div>
	    </div>
    </div>

	<div class="col-md-3">
		<!-- Card Light -->
		<!--	    <div style="width: 230px; height: 230px">-->
		<div class="card" style="width: 200px; height: 200px">
			<div class="view " style="width: 100px; height: 200px">
				<img style="width: 80px; height: 80px" src="https://png.pngtree.com/svg/20170421/day_visitor_876885.png"
				     alt="Card image cap">
				<a>
					<div class="mask rgba-white-slight"></div>
				</a>
			</div>

			<!-- Card content -->
			<div class="card-body">
				<!-- Title -->
				<h6 class="card-title">អ្នកចូលមើលជាប្រចាំ</h6>
				<hr>
				<!-- Text -->
				<p class="card-text text-center">1 billion មើល</p>

			</div>

			<!--		    </div>-->
		</div>
	</div>

	<div class="col-md-3">
		<!-- Card Light -->
		<!--	    <div style="width: 230px; height: 230px">-->
		<div class="card" style="width: 200px; height: 200px">
			<div class="view " style="width: 100px; height: 200px">
				<img style="width: 80px; height: 80px" src="https://cdn1.iconfinder.com/data/icons/communication-283/64/59-512.png"
				     alt="Card image cap">
				<a>
					<div class="mask rgba-white-slight"></div>
				</a>
			</div>
			<!-- Card content -->
			<div class="card-body">
				<!-- Title -->
				<h6 class="card-title">ចំនួនការប្រកាសសរុប</h6>
				<hr>
				<!-- Text -->
				<p class="card-text text-center"><?=$this->post->count_id?> ការប្រកាស</p>

			</div>

			<!--		    </div>-->
		</div>
	</div>

	<div class="col-md-3">
		<!-- Card Light -->
		<!--	    <div style="width: 230px; height: 230px">-->
		<div class="card" style="width: 200px; height: 200px">
			<div class="view " style="width: 100px; height: 200px">
				<img style="width: 80px; height: 80px" src="https://image.flaticon.com/icons/png/512/20/20664.png"
				     alt="Card image cap">
				<a>
					<div class="mask rgba-white-slight"></div>
				</a>
			</div>

			<!-- Card content -->
			<div class="card-body">
				<!-- Title -->
				<h6 class="card-title">ចំនួនចូលចិត្តការប្រកាស់</h6>
				<hr>
				<!-- Text -->
				<p class="card-text text-center">1k ចូលចិត្ត</p>

			</div>

			<!--		    </div>-->
		</div>
	</div>
	
    </div>
	<div style="height: 40px;">
	</div>
</div>






<div class="py-4 row d-flex justify-content-center">
    <div class="col-lg-5 card">
        <div class="view view-cascade   narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
            <a href="" class="text-dark mx-3 h4 font-weight-bold">ស្ថិតិ</a>
            <div>
            </div>
        </div>
        <div style="height;1000px;">
            <canvas id="lineChart"></canvas>
        </div>
    </div>
	<div class="col-lg-7">

		<table id="tablePost" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th scope="col">ចំណងជើង</th>
				<th scope="col">ការពិពណ៌នា</th>
				<th scope="col">រូបភាព</th>
				<th scope="col">កាលបរិច្ឆេទ</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($this->posts as $post):?>
				<tr>
					<td ><a style="color: #4285f4" href="<?=H::resolvePostDetailUrl($post->category_title, $post->id)?>"><?=$post->title?></a></td>
					<td ><?=H::resolveContentLength($post->content, 50)?></td>
					<td class="text-center"><img src="<?=H::resolveImageUrl($post->image_feature)?>" style="width:35px;height:35px;object-fit:contain;"
					                             class="rounded mx-auto d-block" alt="..."></td>
					<td class="text-center"><?=H::resolveFormatPostDate($post->date)?></td>
				</tr>
			<?php endforeach;?>
		</table>
	</div>
</div>
</div>
<!-- example of Input Block -->
<?php //inputBlock('text', 'Username', 'username', 'test', [], ['class' => 'form-control'], ['class' => 'form-group']); ?>
<?php //submitBlock('OK', ['class' => 'btn btn-primary',], ['class' => 'form-group text-right']); ?>

<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script>
//line
var ctxL = document.getElementById("lineChart").getContext('2d');
var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
                label: "My First dataset",
                data: [65, 59, 80, 81, 56, 55, 40],
                backgroundColor: [
                    'rgba(105, 0, 132, .2)',
                ],
                borderColor: [
                    'rgba(200, 99, 132, .7)',
                ],
                borderWidth: 2
            },
            {
                label: "My Second dataset",
                data: [28, 48, 40, 19, 86, 27, 90],
                backgroundColor: [
                    'rgba(0, 137, 132, .2)',
                ],
                borderColor: [
                    'rgba(0, 10, 130, .7)',
                ],
                borderWidth: 2
            }
        ]
    },
    options: {
        responsive: true
    }
});
</script>

<script>
function Employee(name, position, salary, office) {
    this.name = name;
    this.position = position;
    this.salary = salary;
    this._office = office;

    this.office = function() {
        return this._office;
    }
};
$(document).ready(function() {
    $('#dtOrderExample').DataTable({
        "order": [
            [3, "desc"]
        ],
        "language":{
            "lengthMenu": "បង្ហាញ _MENU_ ចំនួនជួរដេក",
            "zeroRecords": "មិនមានទិន្នន័យ",
            "info": "បង្ហាញទំព័រ _PAGE_ នៃ _PAGES_",
            "infoEmpty": "មិនមានទិន្នន័យ",
            "infoFiltered": "(យកចេញ ពី _MAX_ នៃទិន្នន័យសរុប)",
            "search":"ស្វែងរក",
            "paginate":{
                "next":"បន្ទាប់",
                "previous":"ត្រលប់",
                "first":"តំបូង",
                "last":"ចុងក្រោយ",
            }
        }
    });
    $('.dataTables_length').addClass('bs-select');

});
</script>
<?php $this->end(); ?>