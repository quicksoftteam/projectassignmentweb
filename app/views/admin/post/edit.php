<?php $this->setSiteTitle('Category');

use App\Core\H; ?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<link href="<?=PROJECT_PATH?>public/css/summernote-bs4.min.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
<div
    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

    <div>
    </div>

    <a href="" class="white-text mx-3 font-weight-bold h4">កែប្រែការប្រកាស</a>

    <div>
    </div>

</div>
<div class="px-4">
    <form class="text-center" style="color: #757575;" action="<?=PROJECT_PATH?>admin/post/update/<?=$this->post->id?>" method="post" id="formUpdate" enctype="multipart/form-data">
        <input type="hidden" value="<?=$this->post->id?>" name="id">
       <!-- Title -->
       <div class="md-form mt-3">
            <input type="text" id="title" name="title" class="form-control" value="<?=$this->post->title?>">
            <label for="title">ចំណងជើង</label>
        </div>
        <!-- Location -->
        <div class="md-form mt-3 text-left">
            តំណភ្ជាប់លម្អិត: <a href="<?=H::getUrlLinkPostDetail($this->post->category_title, $this->post->id)?>"><?=H::getUrlLinkPostDetail($this->post->category_title, $this->post->id)?></a>
        </div>
        <!-- Description -->
        <div class="md-form mt-3 text-left">
            <textarea name="content"  id="content"  rows="3"><?=$this->post->content?></textarea>
        </div>

        <!-- Category -->
        <div class="text-left pb-1">ប្រភេទនៃការប្រកាស</div>
        <select class="browser-default custom-select mb-4" name="category_id">
            <option value="0" disabled>ជម្រើស</option>
            <?php foreach ($this->categories as $category):?>
                <option value="<?=$category->id?>" <?=$this->post->category_id == $category->id ? 'selected' : ''?>><?=$category->title?></option>
            <?php endforeach;?>
        </select>

        <!--Photo-->
        <div class="md-form">
            <div class="file-field">
                <div class="btn btn-outline-info waves-effect btn-sm float-left">
                    <span>Choose files</span>
                    <input type="file" multiple id="image_feature" name="image_feature">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" placeholder="បញ្ចូលរូបភាព" value="<?=$this->post->image_feature?>">
                </div>
            </div>
        </div>

        <?php if ($this->post->user_type == SUPER_ADMIN_TYPE):?>
        <!-- STATUS -->
        <div class="text-left pb-1">Status</div>
        <select class="browser-default custom-select mb-4" name="status_id">
            <?php foreach ($this->status as $st):?>
                <option value="<?=$st->id?>" <?=$this->post->status_id == $st->id ? 'selected' : ''?>><?=$st->text?></option>
            <?php endforeach;?>
        </select>
        <?php endif;?>

        <!-- Send button -->
        <button class="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect btnUpdate" type="submit">កែប្រែអត្ថបទ</button>

    </form>
    <!-- Form -->

</div>

<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script src="<?=PROJECT_PATH?>public/js/summernote-bs4.min.js"></script>

<script>
$('#content').summernote({
        placeholder: 'ការពិពណ៌នា...',
        tabsize: 2,
        height: 400
      });
</script>
<?php $this->end(); ?>