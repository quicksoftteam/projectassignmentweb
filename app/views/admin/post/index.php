<?php $this->setSiteTitle('Category');
use App\Core\H;
use App\Core\FH;

?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<div
    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

    <div>
    </div>

    <a href="" class="white-text mx-3 h4 font-weight-bold">ការប្រកាស</a>

    <div>
    </div>

</div>
<?php if (FH::isAuthorize('create_post')):?>
<div class="row d-flex justify-content-center">
    <a type="button" class="btn btn-outline-success btn-rounded waves-effect" href="<?=PROJECT_PATH?>admin/post/add"
        data-target=".formAdd">បញ្ចូលការប្រកាស</a>
</div>
<?php endif;?>

<?php if (FH::isAuthorize('delete_post')):?>
<!-- Modal form delete-->
<div class="modal fade formDelete" tabindex="-1" role="dialog" aria-labelledby="formDelete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" id="formDelete">
                <div class="modal-header">
                    <h5 class="modal-title">លុបការប្រកាស: <label name="title" class="title"></label></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    តើអ្នកប្រាកដចិត្តទេថាលុបនូវការប្រកាសមួយនេះ?
                </div>
                <input type="hidden" name="id">
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btnConfirmDelete">លុប</button>
                </div>
        </div>
        </form>
    </div>
</div>
<?php endif;?>
<div class="px-4">

    <table id="tablePost" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">ចំណងជើង
                </th>

                <th class="th-sm">ការពិពណ៌នា
                </th>
                <th class="th-sm">ប្រភេទនៃការប្រកាស
                </th>
                <th class="th-sm">រូបភាព
                </th>
                <th class="th-sm">កាលបរិច្ឆេទ
                </th>
                <th class="th-sm">STATUS
                </th>
                </th>
                <th class="th-sm">សកម្មភាព
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($this->posts as $post):?>
            <tr>
                <input class="id" type="hidden" name="id" value="<?=$post->id?>">
                <td class="title center_td"><?=H::resolveContentLength($post->title, 20)?></td>

                <td class="center_td"><?=H::resolveContentLength($post->content, 50)?></td>
                <td class="text-center center_td"><?=$post->category_title?></td>
                <td>
                    <?php if ($post->image_feature):?>
                    <img src="<?=H::resolveImageUrl($post->image_feature)?>"
                        style="width:30px;height:30px;object-fit:contain;" class="rounded mx-auto d-block" alt="...">
                    <?php endif;?>
                </td>
                <td class="text-center center_td" ><?=H::resolveFormatPostDate($post->date)?></td>
                <?php if (false):?>
                <td class="text-center center_td"><?=$post->status_text?></td>
                <?php elseif (true):?>
                <td  class="text-center center_td">
                    <label class="d-block"><?=$post->status_text?></label>
                    <?php if (FH::isAuthorize('approve_post')):?>
                    <a href="<?=PROJECT_PATH?>admin/post/approve/<?=$post->id?>" class="btn-floating btn-sm waves-effect btn-sm btnApprove" title="ត្រឹមត្រូវ"><i class="fas fa-check text-success"></i></a>
                    <?php endif;?>

                    <?php if (FH::isAuthorize('denied_post')):?>
                    <a href="<?=PROJECT_PATH?>admin/post/denied/<?=$post->id?>" class="btn-floating btn-sm waves-effect btn-sm btnApprove" title="មិនត្រឹមត្រូវ"><i class="fas fa-times text-danger"></i></a>
                    <?php endif;?>

                    <?php if (FH::isAuthorize('waiting_post')):?>
                    <a href="<?=PROJECT_PATH?>admin/post/waiting/<?=$post->id?>" class="btn-floating btn-sm waves-effect btn-sm btnApprove" title="រងចាំត្រួតពិនិត្យ"><i class="fas fa-sync-alt text-warning"></i></a>
                    <?php endif;?>
                </td>
                <?php endif;?>
                <td  class="text-center center_td">
                    <a href="<?=H::resolvePostDetailUrl($post->category_title, $post->id)?>" class="btn-floating btn-sm waves-effect text-center" title="ចុចមើល"><i class="far fa-eye text-info"></i></a>
                    <?php if (FH::isAuthorize('edit_post')):?>
                    <a href="<?=PROJECT_PATH?>admin/post/update/<?=$post->id?>" class="btn-floating btn-sm waves-effect text-center" title="កែប្រែ"><i class="far fa-edit text-success"></i></a>
                    <?php endif;?>

                    <?php if (FH::isAuthorize('delete_post')):?>
                    <a class="btn-floating btn-sm waves-effect btn-sm btnDelete" data-toggle="modal" data-target=".formDelete" title="លុប"><i class="far fa-trash-alt text-danger"></i></a>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <th class="th-sm">ចំណងជើង
                </th>

                <th class="th-sm">ការពិពណ៌នា
                </th>
                <th class="th-sm">ប្រភេទនៃការប្រកាស
                </th>
                <th class="th-sm">រូបភាព
                </th>
                <th class="th-sm">កាលបរិច្ឆេទ
                </th>
                <th class="th-sm">STATUS
                </th>
                </th>
                <th class="th-sm">សកម្មភាព
                </th>
            </tr>
        </tfoot>
    </table>
</div>

<?php $this->end(); ?>

<?php $this->start('script')?>
<?=H::displayMsgToast()?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script>
$(document).ready(function() {
    $('#tablePost').DataTable({
        "order": [
            [3, "desc"]
        ],
        "language":{
            "lengthMenu": "បង្ហាញ _MENU_ ចំនួនជួរដេក",
            "zeroRecords": "មិនមានទិន្នន័យ",
            "info": "បង្ហាញទំព័រ _PAGE_ នៃ _PAGES_",
            "infoEmpty": "មិនមានទិន្នន័យ",
            "infoFiltered": "(យកចេញ ពី _MAX_ នៃទិន្នន័យសរុប)",
            "search":"ស្វែងរក",
            "paginate":{
                "next":"បន្ទាប់",
                "previous":"ត្រលប់",
                "first":"តំបូង",
                "last":"ចុងក្រោយ",
            }
        }
    });
    $('.dataTables_length').addClass('bs-select');

    $('#tablePost').on('click', '.btnDelete', function() {
        let id = $(this).parent().parent().find('.id').val();
        let text = $(this).parent().parent().find('.title').text();
        let form = $('#formDelete');
        form.children('input[name="id"]').val(id);
        form.find('label[name="title"]').html(text);
    });

    $('#btnConfirmDelete').on('click', function(e) {
        e.preventDefault();
        let form = $("#formDelete");
        let id = form.children('input[name="id"]').val();
        $.showSpinner();
        $.post('admin/post/delete/' + id, $(form).serializeArray(), function(result) {
            $.hideSpinner();
            if (result.success) {
                toastr.warning(result.message);
                setTimeout(function() {
                    location.reload();
                }, 1000);
                $('.formDelete').modal('hide');
            } else {
                toastr.error(result.message ?? 'internal error.');
            }

        });
    });

});
</script>
<?php $this->end(); ?>