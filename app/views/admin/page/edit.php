<?php $this->setSiteTitle('Category'); ?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<link href="<?=PROJECT_PATH?>public/css/summernote-bs4.min.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
<div
    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

    <div>
    </div>

    <a href="" class="white-text mx-3 h4 font-weight-bold">កែប្រែទំព័រ</a>

    <div>
    </div>

</div>
<div class="px-4">
    <form class="text-center" style="color: #757575;" action="#" id="formUpdate" method="POST">
        <!-- Title -->
        
        <div class="md-form mt-3">
            <input type="text" name="title" id="title" class="form-control" value="<?=$this->page->title?>" autofocus >
            <label for="title">Title</label>
        </div>
        <!-- Description -->
        <div class="md-form mt-3 text-left">
            <textarea name="content"  id="content"  rows="3"> <?=$this->page->content?></textarea>
        </div>
        <input type="hidden" name="id" value="<?=$this->id;?>">
        
        <!-- Update button -->
        <button class="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect btnUpdate"
            type="submit" name="btnUpdate">Update</button>

    </form>
    <!-- Form -->

</div>

<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>

<script src="<?=PROJECT_PATH?>public/js/summernote-bs4.min.js"></script>
<script>
$('#content').summernote({
        placeholder: 'ការពិពណ៌នា...',
        tabsize: 2,
        height: 400,
      });
$('.btnUpdate').on("click", function(e)
{
    e.preventDefault();
    let form =$("#formUpdate").serializeArray();
    let id = form[3].value;
    if (form[1].value.trim() == ''){
        toastr.error('Please input title.');
        return false;
    }
    $.showSpinner();
    $.post('admin/page/update/'+ id, form, function(result){
        $.hideSpinner();
        if(result.success){
            toastr.success(result.message);
        }else{
            toastr.error(result.message ?? "Internal Error.");
        }
    });
});
</script>


 
<?php $this->end(); ?>