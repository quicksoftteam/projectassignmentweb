<?php 
$this->setSiteTitle('Category'); 
use App\Core\H;
?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<link href="<?=PROJECT_PATH?>public/css/summernote-bs4.min.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>

<?php $this->start('body'); ?>


<!-- Material form contact -->
<div class="card">

    <div
        class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

        <div>
        </div>

        <a href="" class="white-text mx-3 h4 font-weight-bold">បន្ថែមទំព័រថ្មី</a>

        <div>
        </div>

    </div>


    <div class="px-4">

        <!-- Form -->
        <form class="text-center p-0" style="color: #757575;" action="<?=PROJECT_PATH?>admin/page/add" method="POST"
            id="form">

            <!-- Title -->
            <div class="md-form mt-3">
                <input type="text" id="title" name="title" value="<?=$this->page->title;?>" class="form-control" require>
                <label for="title">ចំណងជើង</label>
            </div>
            <!-- Description -->

            <div class="md-form mt-3 text-left">
                <textarea name="content" id="content" rows="3"><?=$this->page->content;?> </textarea>
            </div>
            <!-- Send button -->
            <div class="row d-flex justify-content-center  ">
                <div class="col-md-2 ">
                    <button class="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit"
                        id="btnAdd">បន្ថែមទំព័រថ្មី</button>

                </div>
            </div>


        </form>
        <!-- Form -->
    </div>


</div>
<!-- Material form contact -->

<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script src="<?=PROJECT_PATH?>public/js/summernote-bs4.min.js"></script>
<?=H::displayMsgToast()?>
<script>
$(document).ready(function() {

    $('#content').summernote({
        placeholder: 'ខ្លឹមសារ...',
        tabsize: 2,
        height: 200
    });

});
</script>
<?php $this->end(); ?>