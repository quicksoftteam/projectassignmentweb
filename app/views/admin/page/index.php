<?php 
$this->setSiteTitle('Category');
use App\Core\H;
?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<div
    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

    <div>
    </div>

    <a href="" class="white-text mx-3 h4 font-weight-bold">Page</a>

    <div>
    </div>

</div>
<div class="row d-flex justify-content-center">
    <a type="button" class="btn btn-outline-success btn-rounded waves-effect" href="<?=PROJECT_PATH?>admin/page/add">Add</a>
</div>

<!-- Modal form delete-->
<div class="modal fade formDelete" tabindex="-1" role="dialog" aria-labelledby="formDelete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" id="formDelete">
                <div class="modal-header">
                    <h5 class="modal-title">លុបទំព័រ: <label name="title" class="title"></label></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    តើអ្នកប្រាកដចិត្តទេថាលុបនូវការទំព័រមួយនេះ?
                </div>
                <input type="hidden" name="id">
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btnConfirmDelete">លុប</button>
                </div>
        </div>
        </form>
    </div>
</div>

<div class="px-4">

    <table id="tablePage"  class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">លេខសម្គាល់
                </th>

                <th class="th-sm">ចំណងជើង
                </th>
                <th class="th-sm">មាតិកា
                </th>
                <th class="th-sm">កាលបរិច្ឆេទ
                </th>
                <th class="th-sm">សកម្មភាព
                </th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($this->pages as $page):?>
            <tr>
                <td class="id"><?=$page->id?></td>
                <td class="title"><?=$page->title?></td>
                <td><?=H::resolveContentLength($page->content, 50)?></td>
                
                <td><?=H::resolveFormatPostDate($page->date)?></td>
                <td  class="text-center center_td">
                    <a href="<?=PROJECT_PATH. $page->title?>"
                        class="btn-floating btn-sm waves-effect text-center" title="ចុចមើល"><i
                            class="far fa-eye text-info"></i></a>
                    <a href="<?=PROJECT_PATH?>admin/page/update/<?=$page->id?>"
                        class="btn-floating btn-sm waves-effect text-center" title="កែប្រែ"><i
                            class="far fa-edit text-success"></i></a>
                    <a class="btn-floating btn-sm waves-effect btn-sm btnDelete" data-toggle="modal"
                        data-target=".formDelete" title="លុប"><i class="far fa-trash-alt text-danger"></i></a>
                </td>
            </tr>
        </tbody>
        <?php endforeach;?>
        <tfoot>
            <tr>
            <th class="th-sm">លេខសម្គាល់
                </th>

                <th class="th-sm">ចំណងជើង
                </th>
                <th class="th-sm">មាតិកា
                </th>
                <th class="th-sm">កាលបរិច្ឆេទ
                </th>
                <th class="th-sm">សកម្មភាព
                </th>
            </tr>
        </tfoot>
    </table> 

<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script>
$(document).ready(function() {
    $('#dtOrderExample').DataTable({
        "order": [
            [3, "desc"]
        ],
        "language":{
            "lengthMenu": "បង្ហាញ _MENU_ ចំនួនជួរដេក",
            "zeroRecords": "មិនមានទិន្នន័យ",
            "info": "បង្ហាញទំព័រ _PAGE_ នៃ _PAGES_",
            "infoEmpty": "មិនមានទិន្នន័យ",
            "infoFiltered": "(យកចេញ ពី _MAX_ នៃទិន្នន័យសរុប)",
            "search":"ស្វែងរក",
            "paginate":{
                "next":"បន្ទាប់",
                "previous":"ត្រលប់",
                "first":"តំបូង",
                "last":"ចុងក្រោយ",
            }
        }
    });
    $('.dataTables_length').addClass('bs-select');

});
$('.datepicker').pickadate({
    firstDay: 0
})

$('#tablePage').on('click', '.btnDelete', function() {
        let id = $(this).parent().parent().find('.id').html();
        let text = $(this).parent().parent().find('.title').text();
        let form = $('#formDelete');
        form.children('input[name="id"]').val(id);
        form.find('label[name="title"]').html(text);
    });

    $('#btnConfirmDelete').on('click', function(e) {
        e.preventDefault();
        let form = $("#formDelete");
        let id = form.children('input[name="id"]').val();
        $.showSpinner();
        $.post('admin/page/delete/' + id, $(form).serializeArray(), function(result) {
            $.hideSpinner();
            if (result.success) {
                toastr.warning(result.message);
                setTimeout(function() {
                    location.reload();
                }, 1000);
                $('.formDelete').modal('hide');
            } else {
                toastr.error(result.message ?? 'internal error.');
            }

        });
    });
</script>
<?php $this->end(); ?>