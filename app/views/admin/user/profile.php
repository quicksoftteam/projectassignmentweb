<?php $this->setSiteTitle('Category');

use App\Core\FH;
use App\Core\H; ?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<link href="<?=PROJECT_PATH?>public/css/profile_css.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>
<?php $this->start('body');?>

	<div class="admin-up d-flex justify-content-start">
		<div class="mt-4 mt-4 mt-4">
			<img src="<?= H::resolveImageUrl($this->user_profile->photo)?>" id="img" class=" img-responsive" style="width: 50px; height: 50px">
		</div>
		<div class="data mt-4 mt-4 mt-4 title_profile">
			<span class="sub_title"><h5 class="font-weight-bold dark-grey-text">ប្រវត្តិរូបសង្ខេប</h5></span>

		</div>
	</div>
	<hr class="line_profile">
	<div class="row">
		<div class="col-sm-5">
			<div class="row diplay_magin">
				<div class="col-lg-5">
					<h6><span class="font-weight dark-grey-text text-muted ">ឈ្មោះគណនី </span></h6>
                    <h6><span class="font-weight dark-grey-text text-muted ">នាមត្រកូល</span></h6>
                    <h6><span class="font-weight dark-grey-text text-muted ">នាមខ្លួន</span></h6>
					<h6><span class="font-weight dark-grey-text text-muted ">អ៊ីមែល</span></h6>
					<h6><span class="font-weight dark-grey-text text-muted ">លេខទូរស័ទ្ធ </span></h6>
					<h6><span class="font-weight dark-grey-text text-muted ">ស្ថានភាព </span></h6>
					<h6><span class="font-weight dark-grey-text text-muted ">ប្រភេទអ្នកប្រើប្រាស់ </span></h6>
					<h6><span class="font-weight dark-grey-text text-muted ">ថ្អៃបង្កើត </span></h6>
				</div>
				<div class="col-lg-7">
					<h6 class="font-weight-bold dark-grey-text">: <?=$this->user_profile->username ?></h6>
					<h6 class="font-weight-bold dark-grey-text">: <?=$this->user_profile->lname ?></h6>
					<h6 class="font-weight-bold dark-grey-text">: <?=$this->user_profile->fname ?></h6>
					<h6 class="font-weight-bold dark-grey-text">: <?=$this->user_profile->email ?></h6>
					<h6 class="font-weight-bold dark-grey-text">: <?=$this->user_profile->phone ?></h6>
					<h6 class="font-weight-bold dark-grey-text">: <?=$this->user_profile->deleted ? "អសកម្ម" : "សកម្ម" ?> </h6>
					<h6 class="font-weight-bold dark-grey-text">: <?=$this->user_profile->user_type ?></h6>
					<h6 class="font-weight-bold dark-grey-text">: <?=H::resolveFormatPostDate($this->user_profile->created_date )?> </h6>
				</div>
			</div>
            <div class="diplay_magin_link">
                <a href="<?=PROJECT_PATH?>admin/user/edit/<?=$this->user_profile->id?>">កែរប្រែព័ត៌មានផ្តាល់ខ្លូន</a>
            </div>
			<hr class="line_category">
			<p class="h5 mb-4">ចំណាត់ក្រុម</p>
				
			<div class="row diplay_magin_g">
				<div class="col-lg-5">
					<ul class="list-unstyled">
						<?php foreach ($this->count_category as $category):?>
							<li>
								<a href="<?=PROJECT_PATH?>admin/Post/find/<?=$category->category_id?>"><?=$category->title?></a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="col-lg-7">
					<?php foreach ($this->count_category as $category):?>
						<h6><span class="font-weight text-muted "><?=$category->count_of_curs?></span></h6>
					<?php endforeach; ?>
				</div >
			</div>

		</div>
		<div class="col-sm-7">
			<p class="h5 mb-4">តារាងសង្ខេប</p>


			<!--Accordion wrapper-->
			<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

				<!-- Accordion card -->
				<div class="card">

					<!-- Card header -->
					<div class="card-header" role="tab" id="headingOne1">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
						   aria-controls="collapseOne1">
							<h5 class="mb-0 collapsible-header waves-effect arrow-r subMenu">
								អត្ថបទថ្ងៃនេះ <i class="fas fa-angle-down rotate-icon"></i>
							</h5>
						</a>
					</div>

					<!-- Card body -->
					<div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
						<div class="card-body">

							<table class="table table-striped">
								<thead>
								<tr>
									<th scope="col">ចំណងជើង</th>
									<th scope="col">ការពិពណ៌នា</th>
									<th scope="col">រូបភាព</th>
									<th scope="col">កាលបរិច្ឆេទ</th>
								</tr>
								</thead>
								<tbody>
									<?php foreach ($this->post_list as $post): ?>
										<?php if($post->today): ?>
											<tr>
												<td ><a style="color: #4285f4" href="<?=H::resolvePostDetailUrl($post->category_title, $post->id)?>"><?=$post->title?></a></td>
												<td ><?=H::resolveContentLength($post->content, 50)?></td>
												<td class="text-center"><img src="<?=H::resolveImageUrl($post->image_feature)?>" style="width:35px;height:35px;object-fit:contain;"
												             class="rounded mx-auto d-block" alt="..."></td>
												<td ><?=H::resolveFormatPostDate($post->post_date)?></td>
											</tr>
										<?php endif;?>
									<?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>

				</div>
				<!-- Accordion card -->

				<!-- Accordion card -->
				<div class="card">

					<!-- Card header -->
					<div class="card-header" role="tab" id="headingTwo2">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
						   aria-expanded="false" aria-controls="collapseTwo2">
							<h5 class="mb-0 collapsible-header waves-effect arrow-r subMenu">
								អត្ថម្សិលមិញ <i class="fas fa-angle-down rotate-icon"></i>
							</h5>
						</a>
					</div>

					<!-- Card body -->
					<div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
						<div class="card-body">
							<table class="table table-striped">
								<thead>
								<tr>
									<th scope="col">ចំណងជើង</th>
									<th scope="col">ការពិពណ៌នា</th>
									<th scope="col">រូបភាព</th>
									<th scope="col">កាលបរិច្ឆេទ</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($this->post_list as $post): ?>
									<?php if($post->yesterday): ?>
										<tr>
											<td ><a style="color: #4285f4" href="<?=H::resolvePostDetailUrl($post->category_title, $post->id)?>"><?=$post->title?></a></td>
											<td ><?=H::resolveContentLength($post->content, 50)?></td>
											<td class="text-center"><img src="<?=H::resolveImageUrl($post->image_feature)?>" style="width:35px;height:35px;object-fit:contain;"
											          class="rounded mx-auto d-block" alt="..."></td>
											<td class="text-center"><?=H::resolveFormatPostDate($post->post_date)?></td>
										</tr>
									<?php endif;?>
								<?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- Accordion card -->

				<!-- Accordion card -->
				<div class="card">

					<!-- Card header -->
					<div class="card-header" role="tab" id="headingThree3">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
						   aria-expanded="false" aria-controls="collapseThree3">
							<h5 class="mb-0 collapsible-header waves-effect arrow-r subMenu">
								ថ្ងែផ្សេងៗ <i class="fas fa-angle-down rotate-icon"></i>
							</h5>
						</a>
					</div>

					<!-- Card body -->
					<div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
						<div class="card-body">
							<table class="table table-striped">
								<thead>
								<tr>
									<th scope="col">ចំណងជើង</th>
									<th scope="col">ការពិពណ៌នា</th>
									<th scope="col">រូបភាព</th>
									<th scope="col">កាលបរិច្ឆេទ</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($this->post_list as $post): ?>
									<?php if($post->other_day): ?>
										<tr>
											<td ><a style="color: #4285f4" href="<?=H::resolvePostDetailUrl($post->category_title, $post->id)?>"><?=$post->title?></a></td>
											<td ><?=H::resolveContentLength($post->content, 50)?></td>
											<td class="text-center"><img src="<?=H::resolveImageUrl($post->image_feature)?>" style="width:35px;height:35px;object-fit:contain;"
											          class="rounded mx-auto d-block" alt="..."></td>
											<td class="text-center"><?=H::resolveFormatPostDate($post->post_date)?></td>
										</tr>
									<?php endif;?>
								<?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- Accordion card -->
			</div>
			<!-- Accordion wrapper -->
		</div>
	</div>

<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<?php $this->end(); ?>