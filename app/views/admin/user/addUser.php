<?php $this->setSiteTitle('Category');

use App\Core\H; ?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<link href="<?=PROJECT_PATH?>public/css/user_style.css" rel="stylesheet">
<link href="<?=PROJECT_PATH?>public/css/croppie.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>
<?php $this->start('body');?>
<div
    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
    <div>
    </div>
    <a href="" class="white-text mx-3 h3 h4 font-weight-bold">បន្ថែមអ្នកប្រើប្រាស់</a>
    <div>
    </div>
</div>
<!-- Collapse buttons -->
<?php if($this->currentUser->user_type == SUPER_ADMIN_TYPE): ?>
<div>
    <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false"
        aria-controls="multiCollapseExample1">បង្ហាញគណនីអ្នកប្រើប្រាស់</a>
    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#multiCollapseExample2"
        aria-expanded="false" aria-controls="multiCollapseExample2">បង្ហាញសិទ្ធនៃអ្នកប្រើប្រាស់</button>
    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target=".multi-collapse"
        aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">បង្ហាញទាំងអស់</button>
</div>
<?php endif; ?>
<!--/ Collapse buttons -->

<?php
    // echo "<pre>";
    // var_dump( $this->users);
    // // var_dump($this->user_type);
    // exit();

?>

<!-- Collapsible content -->
<form method="post" enctype="multipart/form-data" action="add">
    <div class="row">
        <div class="col">
            <div class="collapse show multi-collapse" id="multiCollapseExample1">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document">
                            <!--Content-->
                            <div class="modal-content">
                                <!--Header-->
                                <div class="modal-header">
                                    <img src="<?= $this->users->id ? H::resolveImageUrl($this->users->photo) : H::resolveImageUrl("user.png")?>"
                                        id="image" class="rounded-circle img-responsive">
                                </div>
                                <!--Body-->
                                <div class="modal-body text-center mb-1">

                                    <h5 class="mt-1 mb-2">អ្នកគ្រប់គ្រង</h5>

                                    <div class="md-form">
                                        <div class="file-field">
                                            <div class="btn btn-outline-info waves-effect btn-sm float-left">
                                                <span>ជ្រើសរើសរូបភាព</span>
                                                <input type="file" name="photo" id="photo" />
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" id="photo-name" type="text"
                                                    placeholder="Name file">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--/.Content-->
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <br>
                        <div class="text-center">
                            <p class="h4 font-weight-bold">ព័ត៌មានអ្នកប្រើប្រាស់</p>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col">
                                <div class="md-form">
                                    <input type="text" id="inputIconEx1" class="form-control" name="username"
                                        value="<?=$this->users->username?>">
                                    <label for="inputIconEx1">ឈ្មោះ</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="md-form">
                                    <input type="text" id="inputIconEx1" class="form-control" name="fname"
                                        value="<?=$this->users->fname?>">
                                    <label for="inputIconEx1">នាមខ្លួន</label>
                                </div>
                            </div>

                            <div class="col">
                                <div class="md-form">
                                    <input type="text" id="inputIconEx1" class="form-control" name="lname"
                                        value="<?=$this->users->lname?>">
                                    <label for="inputIconEx1">នាមត្រកូល</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="md-form">
                                    <input type="email" id="materialContactFormEmail" class="form-control" name="email"
                                        value="<?=$this->users->email?>">
                                    <label for="materialContactFormEmail">អ៊ីមែល</label>
                                </div>
                            </div>

                            <div class="col">
                                <div class="md-form">
                                    <input type="text" id="inputIconEx1" class="form-control" name="phone"
                                        value="<?=$this->users->phone?>">
                                    <label for="materialContactFormPhone">លេខទូរស័ព្ទ</label>
                                </div>
                            </div>
                        </div>

                        <?php  if(!($this->users->id)): ?>
                        <div class="row">
                            <div class="col">
                                <div class="md-form form-inline mt-0">
                                    <input type="password" id="inputPassword6MD" class="form-control mr-sm-3"
                                        aria-describedby="passwordHelpInlineMD" name="password">
                                    <label for="inputPassword6MD">លេខសម្ងាត់</label>
                                    <small id="passwordHelpInlineMD" class="text-muted">
                                        ត្រូវមានប្រវែង ៨-២០ តួ
                                    </small>
                                </div>
                            </div>

                            <div class="col">
                                <div class="md-form form-inline mt-0">
                                    <input type="password" id="inputPassword6MD" class="form-control mr-sm-3"
                                        aria-describedby="passwordHelpInlineMD">
                                    <label for="inputPassword6MD">
                                        ពាក្យសម្ងាត់បញ្ជាក់</label>
                                    <small id="passwordHelpInlineMD" class="text-muted">
                                        ត្រូវមានប្រវែង ៨-២០ តួ
                                    </small>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                        <div class="row">
                            <div class="col-sm-5">
                                <span>ប្រភេទអ្នកប្រើប្រាស់</span>
                                <select class="browser-default custom-select mb-4" name="user_type">
                                    <option value="" selected disabled>ជម្រើស</option>
                                    <?php foreach($this->user_type as $id=>$type):
                                        $idu = $id+1;  
                                        $select = ($idu == $this->users->user_type ? "selected" :" ");
                                    ?>
                                    <?='<option value="'.$idu.'"'.$select.' >'.$type->type.'</option>'?>
                                    <?php endforeach?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <?php if($this->users->id): ?>
                                <span class="display-block">ស្ថានភាព គណនី</span>
                                <!-- true -->
                                <div class="form-check magin-vertical form-check-inline ">
                                    <input type="radio" checked class="form-check-input" value="" id="deleteTrue" name="deleted">
                                    <label class="form-check-label" for="deleteTrue">Active</label>
                                </div>
                                <!-- false-->
                                <div class="form-check magin-vertical form-check-inline">
                                    <input type="radio" class="form-check-input" value="" id="deleteFalse" name="deleted">
                                    <label class="form-check-label" for="deleteFalse">Deactivated</label>
                                </div>
                                <?php endif;?>
                            </div>
                            <div class="col-sm-3">
                                <?php if($this->users->id): ?>
                                <span class="display-block">&nbsp;</span>
                                <div class="form-check magin-vertical form-check-inline ">
                                    <a href="#!" class="card-link magin-vertical" data-toggle="modal"
                                        data-target=".change-password">ផ្លាស់ប្តូរពាក្យសម្ងាត់</a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="collapse show multi-collapse" id="multiCollapseExample2">
                <div class="card card-body">
                    <div>
                        <p class="h4 font-weight-bold">
                            តួនាទីអ្នកប្រើ</p>
                        <hr>
                    </div>
                    <?php $role = (array)$this->users_Roles;?>
                    <?php
                        $role = array_diff_key($role, ['id' => null, 'user_id'=>null, 'created_date_roles'=>null]);
                        $i=0;
                        foreach($role as $r=>$value):
                            $str = str_replace("_"," ",$r); 
                            $Name_roles = ucwords($str);
                        ?>
                    <div class="row justify-content-start">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-3">
                            <label
                                class="form-check-label"><?="Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;". $Name_roles?></label>
                        </div>
                        <div class="col-sm-6">
                            <?php 
                                        $class_show = $value == 1 ? "user-active" : "user-deactivate";
                                        $Name_show = $value == 1 ? "Enabled" : "Disabled";  
                                        $check = $value == 1 ? "checked" : "";
                                        $value_check = $value == 1 ? 1 : 0 ;
                                    ?>
                            <div class="form-check form-check">
                                <input type="checkbox" name="<?=$r?>" class="form-check-input" <?=$check?>
                                    value="<?=$value_check ?>" onclick="changeValue(<?=$i?>)" id='<?=$i;?>'>
                                <label id="text<?=$i?>" class="form-check-label <?=$class_show?>"
                                    for='<?=$i;?>'><?=$Name_show?></label>
                            </div>
                        </div>
                    </div>
                    <?php $i++; endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="text-right magin_bottom_form magin_top_form_botton">
        <input class="btn btn-primary" type="submit" value="Submit">
    </div>
</form>

<div class="modal fade change-password" tabindex="-1" role="dialog" aria-labelledby="change-password"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- Material form contact -->
            <div class="card">
                <div
                    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                    <div></div>
                    <a href="" class="white-text mx-3 h3 display-6 font-weight-bold">ផ្លាស់ប្តូរពាក្យសម្ងាត់</a>
                    <div></div>
                </div>
                <!--Card content-->
                <div class="card-body px-lg-5 pt-0">
                    <!-- Form -->
                    <form style="color: #757575;" id="formAdd" class="text-center needs-validation" novalidate>
                        <div class="md-form form-inline mt-0">
                            <input type="password" id="inputPassword6MD" class="form-control mr-sm-3"
                                aria-describedby="passwordHelpInlineMD" name="old_password">
                            <label for="inputPassword6MD">លេខសំងាត់​ចាស់</label>
                        </div>
                        <div class="md-form form-inline mt-0">
                            <input type="password" id="inputPassword6MD" class="form-control mr-sm-3"
                                aria-describedby="passwordHelpInlineMD" name="new_password">
                            <label for="inputPassword6MD">
                                ពាក្យសម្ងាត់​ថ្មី</label>
                        </div>
                        <div class="md-form form-inline mt-0">
                            <input type="password" id="inputPassword6MD" class="form-control mr-sm-3"
                                aria-describedby="passwordHelpInlineMD" name="comf_password">
                            <label for="inputPassword6MD">
                                បញ្ជាក់ពាក្យសម្ងាត់ថ្មី</label>
                        </div>
                        <!-- Send button -->
                        <button id="btnAdd"
                            class="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect"
                            type="submit">
                            ផ្លាស់ប្តូរ</button>

                    </form>
                    <!-- Form -->
                </div>
            </div>
            <!-- Material form contact -->
        </div>
    </div>
</div>



<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">អាប់លោត & សារ៉េរូបភាព</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <div id="image_demo" style="width:350px; margin: 0 auto;"></div>
                    </div>
                    <div class="col-md-5 text-center">
                        <button class="btn btn-success crop_image" style="margin-top: 50%;">អាប់លោត & សារ៉េរូបភាព</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">បិទ</button>
            </div>
        </div>
    </div>
</div>
<?php $this->end(); ?>


<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/croppie.js"></script>
<script>
function changeValue(id) {
    var checkBox = document.getElementById(id);
    var text = document.getElementById("text" + id);
    if (checkBox.checked == true) {
        text.classList.add("user-active");
        text.classList.remove("user-deactivate");
        checkBox.value = 1;
        text.innerHTML = 'Enabled';
    } else {
        text.classList.add("user-deactivate");
        text.classList.remove("user-active");
        checkBox.value = 0;
        text.innerHTML = 'Disabled';
    }
}


$(document).ready(function() {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');

    $image_crop = $('#image_demo').croppie({
        enableExif: true,
        viewport: {
            width: 250,
            height: 250,
            type: 'square' //circle
        },
        boundary: {
            width: 350,
            height: 350
        }
    });

    $('#photo').on('change', function() {
        var reader = new FileReader();
        reader.onload = function(event) {
            $image_crop.croppie('bind', {
                url: event.target.result
            }).then(function() {
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
        $('#uploadimageModal').modal('show');
    });

    $('.crop_image').click(function(event) {
        $image_crop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function(response) {
            $.ajax({
                url: "upLoad",
                type: "POST",
                data: {
                    "image": response
                },
                success: function(data) {
                    // console.log(data);
                    $('#image').attr('src', data);
                    $('#uploadimageModal').modal('hide');
                    // $('#uploaded_image').html(data);
                }
            });
        })
    });
});
</script>
<?php $this->end();