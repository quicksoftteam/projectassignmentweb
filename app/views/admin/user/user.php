<?php $this->setSiteTitle('Category');

use App\Core\FH;
use App\Core\H; ?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<link href="<?=PROJECT_PATH?>public/css/user_style.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>
<?php $this->start('body');?>

<div
    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

    <div>
    </div>

    <a href="" class="white-text mx-3 h4 font-weight-bold">អ្នកប្រើប្រាស់់</a>

    <div>
    </div>

</div>
<div class="row d-flex justify-content-center px-4">
    <a type="button" class="btn btn-outline-success btn-rounded waves-effect" href="<?=PROJECT_PATH?>admin/user/add"
        data-target=".formAdd">បន្ថែមអ្នកប្រើប្រាស់ថ្មី</a>
</div>


<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="th-sm">ឈ្មោះ
            </th>
            <th class="th-sm">អ៊ីមែល
            </th>
            <th class="th-sm">តួនាទី
            </th>
            <th class="th-sm">រូបភាពប្រូហ្វាល់
            </th>
            <th class="th-sm">
                កាលបរិច្ឆេទដែលបង្កើត
            </th>
            <th class="th-sm">STATUS
            </th>
            <th class="th-sm">សកម្មភាព
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($this->users as $user): ?>
        <tr>
            <td>
                <?=$user->username ?>
            </td>
            <td>
                <?=$user->email?>
            </td>
            <td>
                <?=$user->type?>
            </td>
            <td>
                <img src="<?=H::resolveImageUrl($user->photo)?>" style="width:35px;height:35px;object-fit:contain;"
                    class="rounded mx-auto d-block" alt="...">
            </td>
            <td>
	            <?=H::resolveFormatPostDate($user->created_date) ?>
            </td>
            <td <?=$user->deleted ? 'class ="user-deactivate"' : 'class ="user-active"'?>>
                <?=$user->deleted ? "Deactivate" : "Active" ?>
            </td>

            <td  class="text-center center_td">
                <?php $icon = $btn_name = $user->deleted ? "fa-user-alt-slash text-danger" : "fa-user text-primary"; ?>
                <?php if (FH::isAuthorize('update_user')): ?>
                <a href="<?=PROJECT_PATH?>admin/user/edit/<?=$user->id?>"
                        class="btn-floating btn-sm waves-effect text-center" title="ចុចមើល"><i
                            class="far fa-eye text-info"></i></a>
                <?php endif; ?>

                <?php if (FH::isAuthorize('disable_user')): ?>
                    <a href="<?=PROJECT_PATH?>admin/user/disable/<?=$user->id."/".$user->deleted?>"
                        class="btn-floating btn-sm waves-effect text-center" title="ចុចមើល"><i
                            class="fas <?=$icon?>"></i></a>
                <?php endif; ?>
                </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php $this->end(); ?>


<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script>
$(document).ready(function() {
    $('#dtBasicExample').DataTable({
        "language":{
            "lengthMenu": "បង្ហាញ _MENU_ ចំនួនជួរដេក",
            "zeroRecords": "មិនមានទិន្នន័យ",
            "info": "បង្ហាញទំព័រ _PAGE_ នៃ _PAGES_",
            "infoEmpty": "មិនមានទិន្នន័យ",
            "infoFiltered": "(យកចេញ ពី _MAX_ នៃទិន្នន័យសរុប)",
            "search":"ស្វែងរក",
            "paginate":{
                "next":"បន្ទាប់",
                "previous":"ត្រលប់",
                "first":"តំបូង",
                "last":"ចុងក្រោយ",
            }
        }
    });
    $('.dataTables_length').addClass('bs-select');


    $("#send").click(function() {
        $.ajax({
            type: 'POST',
            success: function(data) {
                if (data != "Error") {
                    $.post('admin/user/test2', {
                        "name": $("#materialContactFormName").val()
                    }, function(result) {
                        console.log(result);
                    });
                    window.location.href = "test1";
                }
            }
        });
    });
});
</script>
<?php $this->end();