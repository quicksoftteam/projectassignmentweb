<?php
use App\Core\Dom;
use App\Core\FH;
use App\Core\H;
use App\Core\Router;

?>

 <!-- Navbar -->
 <nav class="navbar fixed-top navbar-expand-lg scrolling-navbar double-nav p-1">

<!-- SideNav slide-out button -->
<div class="float-left">
  <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
</div>
<!-- Breadcrumb
<div class="breadcrumb-dn mr-auto">
  <p>Tables extended</p>
</div> -->

<!-- Navbar links -->
<ul class="nav navbar-nav nav-flex-icons ml-auto">

  <!-- Dropdown -->
  <li class="nav-item dropdown notifications-nav">
	<a class="nav-link dropdown-toggle waves-effect" id="navbarDropdownMenuLink" data-toggle="dropdown"
	  aria-haspopup="true" aria-expanded="false">
	  <span class="badge red">3</span> <i class="fas fa-bell"></i>
	  <span class="d-none d-md-inline-block h6">សារដំណឹង</span>
	</a>
	<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
	  <a class="dropdown-item" href="#">
		<i class="far fa-money-bill-alt mr-2" aria-hidden="true"></i>
		<span>New order received</span>
		<span class="float-right"><i class="far fa-clock" aria-hidden="true"></i> 13 min</span>
	  </a>
	  <a class="dropdown-item" href="#">
		<i class="far fa-money-bill-alt mr-2" aria-hidden="true"></i>
		<span>New order received</span>
		<span class="float-right"><i class="far fa-clock" aria-hidden="true"></i> 33 min</span>
	  </a>
	  <a class="dropdown-item" href="#">
		<i class="fas fa-chart-line mr-2" aria-hidden="true"></i>
		<span>Your campaign is about to end</span>
		<span class="float-right"><i class="far fa-clock" aria-hidden="true"></i> 53 min</span>
	  </a>
	</div>
  </li>
  <li class="nav-item">
	<a class="nav-link waves-effect"><i class="fas fa-envelope"></i> <span class="clearfix d-none d-sm-inline-block h6">ទំនាក់ទំនង</span></a>
  </li>
  <li class="nav-item">
	<a class="nav-link waves-effect"><i class="far fa-comments"></i> <span class="clearfix d-none d-sm-inline-block h6">ជំនួយ</span></a>
  </li>
  <li class="nav-item dropdown">
	<a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"
	  aria-haspopup="true" aria-expanded="false">
		<img src="<?= H::resolveImageUrl($this->currentUser->photo) ?>" id="img" class="rounded-circle img-responsive" style="width: 25px; height: 25px"></i>
		<span class="clearfix d-none d-sm-inline-block h6">ប្រវត្តិរូប</span>
	</a>
	 
	<div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
		<a class="dropdown-item" href="<?=PROJECT_PATH?>admin/user/profile">គណនីរបស់ខ្ញុំ</a>
	    <a class="dropdown-item" href="<?=PROJECT_PATH?>register/logout">ចាកចេញ</a>
		
	</div>
  </li>

</ul>
<!-- Navbar links -->

</nav>
<!-- Navbar -->