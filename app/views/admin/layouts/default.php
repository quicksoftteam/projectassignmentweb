<?php

use App\Core\FH;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        <?=$this->_siteTitle;?>
    </title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link href="<?=PROJECT_PATH?>public/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=PROJECT_PATH?>public/css/mdb.min.css" rel="stylesheet">
    <link href="<?=PROJECT_PATH?>public/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Hanuman&display=swap" rel="stylesheet">
    <?=$this->content('head'); ?>
</head>

<body class="fixed-sn white-skin">

    <!-- Main Navigation -->
    <header>

        <!-- Sidebar navigation -->
        <div id="slide-out" class="side-nav sn-bg-c fixed">
            <ul class="custom-scrollbar">

                <!-- Logo -->
                <li class="waves-effect px-5 pb-5 border-bottom">
                    <div class="text-center">
                        <a href="<?=PROJECT_PATH?>" class="pl-0">
                            <img style="object-fit: contain; height:100px;" src="<?=PROJECT_PATH?>public/img/admin/logo.png">
                        </a>
                    </div>
                </li>

                <!-- Side navigation links -->
                <li>
                    <ul class="collapsible collapsible-accordion">
                        <li>
                            <a class="collapsible-header waves-effect arrow-r subMenu" href="<?=PROJECT_PATH?>admin/Dashboard/">
                                <i class="w-fa fas fa-tachometer-alt"></i>ផ្ទាំងព័ត៌មាន
                            </a>
                        </li>
<!--                        --><?php //if($this->currentUser->user_type == SUPER_ADMIN_TYPE): ?>

                        <?php if (FH::isAuthorize(['view_user', 'create_user', 'update_user', 'disable_user'])):?>
                            <li>
                                <a class="collapsible-header waves-effect arrow-r subMenu">
                                    <i class="w-fa far fa-user"></i>អ្នកប្រើប្រាស់
                                    <i class="fas fa-angle-down rotate-icon"></i>
                                </a>
                                <div class="collapsible-body">
                                    <ul>
                                        <!--going to do-->
                                        <?php if (FH::isAuthorize(['view_user', 'create_user', 'update_user', 'disable_user'])):?>
                                        <li>
                                            <a href="<?=PROJECT_PATH?>admin/user" class="waves-effect">តារាងអ្នកប្រើប្រាស់</a>
                                        </li>
                                        <?php endif; ?>
                                        <?php if (FH::isAuthorize('create_user')):?>
                                        <li>
                                            <a href="<?=PROJECT_PATH?>admin/user/add" class="waves-effect">បន្ថែមអ្នកប្រើប្រាស់ថ្មី</a>
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </li>
                        <?php endif; ?>
                        <?php if (FH::isAuthorize(['create_post','edit_post', 'delete_post', 'approve_post', 'waiting_post', 'denied_post', 'view_all_post'])):?>
                        <li>

                            <a class="collapsible-header waves-effect arrow-r subMenu">
                                <i class="w-fa fas fa-bolt"></i>ការប្រកាស
                                <i class="fas fa-angle-down rotate-icon"></i>
                            </a>
                            <div class="collapsible-body">
                                <ul>
                                    <?php if (FH::isAuthorize(['view_all_post','edit_post', 'delete_post', 'approve_post', 'waiting_post', 'denied_post'])):?>
                                    <li> <a href="<?=PROJECT_PATH?>admin/post" class="waves-effect">តារាងការប្រកាស</a> </li>
                                    <?php endif;?>

                                    <?php if (FH::isAuthorize('create_post')):?>
                                    <li><a href="<?=PROJECT_PATH?>admin/post/add" class="waves-effect">បន្ថែមការប្រកាសថ្មី</a></li>
                                    <?php endif;?>
                                </ul>
                            </div>
                        </li>
                        <?php endif;?>
                        <li>
                            <a href="<?=PROJECT_PATH?>admin/comment" class="collapsible-header waves-effect​ subMenu">
                                <i class="fas fa-comments"></i>វិចារ</a>
                        </li>
                        <?php if (FH::isAuthorize(['view_all_category', 'create_category', 'edit_category', 'delete_category'])):?>
                        <li>
                            <a href="<?=PROJECT_PATH?>admin/category" class="collapsible-header waves-effect subMenu">
                                <i class="w-fa fas fa-bars"></i>ប្រភេទនៃអត្ដបទ</a>
                        </li>
                        <?php endif;?>
                        <li>
                            <a class="collapsible-header waves-effect arrow-r subMenu">
                                <i class="w-fa far fa-address-book"></i>ទំព័រ
                                <i class="fas fa-angle-down rotate-icon"></i>
                            </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li>
                                        <a href="<?=PROJECT_PATH?>admin/page" class="waves-effect">ចុចមើលលើតារាងគេហទំព័រ</a>
                                    </li>
                                    <li>
                                        <a href="<?=PROJECT_PATH?>admin/page/add" class="waves-effect">បន្ថែមគេហទំព័រថ្មី</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a class="collapsible-header waves-effect arrow-r subMenu">
                                <i class="w-fa far fa fa-cogs" aria-hidden="true"></i>ការកំណត់
                                <i class="fas fa-angle-down rotate-icon"></i>
                            </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li>
                                        <a href="<?=PROJECT_PATH?>admin/setting" class="collapsible-header waves-effect">
                                            <i class="w-fa far fa-address-book"></i>ម៉ឺនុយ</a>
                                    </li>
                                    <li>
                                        <a href="../dashboards/v-2.html" class="waves-effect">Version 2</a>
                                    </li>
                                    <li>
                                        <a href="../dashboards/v-3.html" class="waves-effect">Version 3</a>
                                    </li>
                                    <li>
                                        <a href="../dashboards/v-4.html" class="waves-effect">Version 4</a>
                                    </li>
                                    <li>
                                        <a href="../dashboards/v-5.html" class="waves-effect">Version 5</a>
                                    </li>
                                    <li>
                                        <a href="../dashboards/v-6.html" class="waves-effect">Version 6</a>
                                    </li>
                                </ul>
                            </div>

                        </li>

                    </ul>
                </li>
                <!-- Side navigation links -->

            </ul>
            <div class="sidenav-bg mask-strong">
            </div>
        </div>
        <!-- Sidebar navigation -->

        <?php include 'main_menu.php'; ?>

    </header>
    <!-- Main Navigation -->

    <!-- Main layout -->
    <main class="m-0 m-2" style="background-color: #eee!important;">
        <div class="card card-cascade z-depth-1 container-fluid m-0">
            <?=$this->content('body'); ?>
        </div>
    </main>
    <!-- Main layout -->
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?=PROJECT_PATH?>public/js/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?=PROJECT_PATH?>public/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?=PROJECT_PATH?>public/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?=PROJECT_PATH?>public/js/mdb.min.js"></script>

    <!-- custom script -->
    <script type="text/javascript">
        let project_path = '<?=PROJECT_PATH?>';
    </script>
    <script type="text/javascript" src="<?=PROJECT_PATH?>public/js/script.js"></script>
    <script type="text/javascript" src="<?=PROJECT_PATH?>public/js/loadingoverlay.min.js"></script>
    <!-- Custom scripts -->
    <script>
        // SideNav Initialization
        $(".button-collapse").sideNav();

        var container = document.querySelector('.custom-scrollbar');
        // Ps.initialize(container, {
        //   wheelSpeed: 2,
        //   wheelPropagation: true,
        //   minScrollbarLength: 20
        // });

        // Material Select Initialization
        $(document).ready(function () {
            $('.mdb-select').material_select();
        });

        $(window).on('beforeunload', function () {
            return $.showSpinner();
        });

        $(document).ready(function () {
            $.hideSpinner();
        })
    </script>
    <?=$this->content('script');?>
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "acb707c5-b338-48a6-ad3b-e25778e43196",
                notifyButton: {
                    enable: true,
                },
            });
        });
    </script>
</body>

</html>