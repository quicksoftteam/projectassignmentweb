<?php $this->setSiteTitle('Category'); ?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<style>
th {
    text-align: center
}
</style>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
<div
    class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

    <div>
    </div>

    <a href="" class="white-text mx-3 h4 font-weight-bold">វិចារ</a>

    <div>
    </div>

</div>


<div class="px-4">


    <table id="dtOrderExample" style="table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
bottom: .5em;
}" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">
                    អ្នកវិចារ
                </th>
                <th class="th-sm">វិចារ
                </th>
                <th class="th-sm">លេខសម្គាល់
                </th>
                <th class="th-sm">
                    ឆ្លើយតបទៅកាន់
                </th>
                <th class="th-sm">
                បានដាក់នៅលើ
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>John Will</td>
                <td>Nothing Just doing a test</td>
                <td>123</td>
                <td>
                    <p> Angkor wat is ....</p> <a href="#" class="text-info">View Post</a>
                </td>
                <td>
                    30/02/19
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
            <th class="th-sm">
                    អ្នកវិចារ
                </th>
                <th class="th-sm">វិចារ
                </th>
                <th class="th-sm">លេខសម្គាល់
                </th>
                <th class="th-sm">
                    ឆ្លើយតបទៅកាន់
                </th>
                <th class="th-sm">
                បានដាក់នៅលើ
                </th>
            </tr>
        </tfoot>
    </table>

</div>
<!-- example of Input Block -->
<?php //inputBlock('text', 'Username', 'username', 'test', [], ['class' => 'form-control'], ['class' => 'form-group']); ?>
<?php //submitBlock('OK', ['class' => 'btn btn-primary',], ['class' => 'form-group text-right']); ?>

<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script>
$(document).ready(function() {
    $('#dtOrderExample').DataTable({
        "order": [
            [3, "desc"]
        ],
        "language":{
            "lengthMenu": "បង្ហាញ _MENU_ ចំនួនជួរដេក",
            "zeroRecords": "មិនមានទិន្នន័យ",
            "info": "បង្ហាញទំព័រ _PAGE_ នៃ _PAGES_",
            "infoEmpty": "មិនមានទិន្នន័យ",
            "infoFiltered": "(យកចេញ ពី _MAX_ នៃទិន្នន័យសរុប)",
            "search":"ស្វែងរក",
            "paginate":{
                "next":"បន្ទាប់",
                "previous":"ត្រលប់",
                "first":"តំបូង",
                "last":"ចុងក្រោយ",
            }
        }
    });
    $('.dataTables_length').addClass('bs-select');

});
</script>
<?php $this->end(); ?>