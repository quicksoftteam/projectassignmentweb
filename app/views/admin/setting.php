<?php $this->setSiteTitle('Home'); ?>

<?php $this->start('head'); ?>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
         <!--
            <div class="row">
                <div class="col-md-6">
                    <div class="card mb-3">
                        <div class="card-header"><h5 class="float-left text-white">Menu</h5>
                            <div class="float-right">
                                <button id="btnReload" type="button" class="btn btn-white">
                                    <i class="fa fa-play"></i> Load Data</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul id="myEditor" class="sortableLists list-group">
                            </ul>
                        </div>
                    </div>
                    <p>Click the Output button to execute the function <code>getString();</code></p>
                    <div class="card">
                    <div class="card-header text-white">JSON Output
                    <div class="float-right">
                    <button id="btnOutput" type="button" class="btn btn-success"><i class="fas fa-check-square"></i> Output</button>
                    </div>
                    </div>
                    <div class="card-body">
                    <div class="form-group"><textarea id="out" class="form-control" cols="50" rows="10"></textarea>
                    </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-primary mb-3">
                        <div class="card-header bg-primary text-white">Edit item</div>
                        <div class="card-body">
                            <form id="frmEdit" class="form-horizontal">
                                <div class="form-group">
                                    <label for="text">Text</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control item-menu" name="text" id="text" placeholder="Text">
                                        <div class="input-group-append">
                                            <button type="button" id="myEditor_icon" class="btn btn-white btn-sm"></button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="icon" class="item-menu">
                                </div>
                                <div class="form-group">
                                    <label for="href">URL</label>
                                    <input type="text" class="form-control item-menu" id="href" name="href" placeholder="URL">
                                </div>
                                <div class="form-group">                              
                                   <span>Target</span>
                                        <select name="target" id="target" class="browser-default custom-select mb-4">
                                        <option value="_self">Self</option>
                                        <option value="_blank">Blank</option>
                                        <option value="_top">Top</option>
                                   </select>

                                </div>
                                <div class="form-group">
                                    <label for="title">Tooltip</label>
                                    <input type="text" name="title" class="form-control item-menu" id="title" placeholder="Tooltip">
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                            <button type="button" id="btnUpdate" class="btn btn-primary" disabled><i class="fas fa-sync-alt"></i> Update</button>
                            <button type="button" id="btnAdd" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                        </div>
                    </div>
             
                </div>
            </div>
          -->

    <div class="d-flex flex-column">
        <div class="p-2 flex-fill bg-danger white-text">Flex item 1</div>
    </div>
    <div class="d-flex flex-row">
        <div class="card-deck">
            <!-- Card Dark -->
            <div class="card mb-4">

                <!-- Card content -->
                <div class="card-body elegant-color white-text rounded-bottom">
                    <!-- Title -->
                    <h4 class="card-title">Card title</h4>
                    <hr class="hr-light">
                    <!-- Text -->
                    <p class="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <!-- Link -->

                </div>

            </div>
            <!-- Card Dark -->
        </div>
        <div class="card-deck">
            <!-- Card Dark -->
            <div class="card mb-4">

                <!-- Card content -->
                <div class="card-body elegant-color white-text rounded-bottom">
                    <!-- Title -->
                    <h4 class="card-title">Card title</h4>
                    <hr class="hr-light">
                    <!-- Text -->
                    <p class="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <!-- Link -->

                </div>
            </div>
            <!-- Card Dark -->
        </div>

    </div>
 <!-- example of Input Block -->
<?php //inputBlock('text', 'Username', 'username', 'test', ['class' => 'form-control'], ['class' => 'form-group']); ?>
<?php //submitBlock('OK', ['class' => 'btn btn-primary',], ['class' => 'form-group text-right']); ?>

<?php $this->end(); ?>

<?php $this->start('script')?>
<script>

</script>
<?php $this->end(); ?>


