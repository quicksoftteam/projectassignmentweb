<?php $this->setSiteTitle($this->post->title);

use App\Core\H; ?>

<?php $this->start('body');?>
<div class="container">
    <!-- center-in -->
    <div class="container min-vh-50">
        <!-- Projects section v.3 -->
        <section class="my-5">
            <!-- Section heading -->
            <h2 class="h1-responsive font-weight-bold text-left "><?=$this->post->title?></h2>
            <p class="d-flex">Posted by <?=$this->post->username?>, <?=H::resolveFormatPostDate($this->post->date)?>, &nbsp;
                <a href="<?=H::resolvePostListUrl($this->post->category_title)?>" class="btn-link"><?=$this->post->category_title?></a>
            </p>
            <br>
            <!-- Section description -->
            <!-- Grid row -->
            <div class="row">
                <?=H::stringToHTML($this->post->content)?>
            </div>
            <hr class="my-5">

            <!-- Grid row -->
        </section>
        <!-- Projects section v.3 -->
    </div>
    <!--end center-in -->
</div>
<?php $this->end();?>
