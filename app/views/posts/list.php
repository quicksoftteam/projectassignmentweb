<?php $this->setSiteTitle('List Posts');

use App\Core\H; ?>

<?php $this->start('body');?>
<div class="container">

    <div class="container">
        <!-- Section: Blog v.3 -->
        <section class="my-5">

            <!-- Section heading -->
            <h2 class="h1-responsive font-weight-bold text-center my-5"><?=$this->list[0]->category_title?></h2>
            <!-- Section description -->
            <?php foreach ($this->list as $post):?>
                <!-- Grid row -->
                <div class="row">
                    <!-- Grid column -->
                    <div class="col-lg-5 col-xl-4">
                        <!-- Featured image -->
                        <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4">
                            <img class="img-fluid" src="<?=H::resolveImageUrl($post->image_feature)?>"
                                 alt="Bokor">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="col-lg-7 col-xl-8">
                        <!-- Post title -->
                        <h3 class="font-weight-bold mb-3"><strong><?=$post->title?></strong></h3>
                        <!-- Excerpt -->
                        <p class="dark-grey-text"><?=H::resolveContentLength($post->content, 300)?></p>
                        <!-- Post data -->
                        <p>by <a class="font-weight-bold"><?=$post->username?></a>, <?=H::resolveFormatPostDate($post->date)?></p>
                        <!-- Read more button -->
                        <a class="btn btn-primary btn-md"​ href="<?=H::resolvePostDetailUrl($post->category_title, $post->id)?>">អានបន្ថែម</a>
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
                <hr class="my-5">
            <?php endforeach;?>
        </section>
        <!-- Section: Blog v.3 -->

        <nav aria-label="Page navigation">
            <ul class="pagination pagination-circle pg-blue justify-content-center">
                <li class="page-item disabled"><a class="page-link">First</a></li>
                <li class="page-item disabled">
                    <a class="page-link" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item active"><a class="page-link" href="<?=H::resolvePostListUrl($post->category_title).'?p=1'?>">1</a></li>
                <li class="page-item"><a class="page-link" href="<?=H::resolvePostListUrl($post->category_title).'?p=2'?>">2</a></li>
                <li class="page-item"><a class="page-link" href="<?=H::resolvePostListUrl($post->category_title).'?p=3'?>">3</a></li>
                <li class="page-item"><a class="page-link" href="<?=H::resolvePostListUrl($post->category_title).'?p=4'?>">4</a></li>
                <li class="page-item"><a class="page-link" href="<?=H::resolvePostListUrl($post->category_title).'?p=5'?>">5</a></li>
                <li class="page-item">
                    <a class="page-link" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
                <li class="page-item"><a class="page-link">Last</a></li>
            </ul>
        </nav>
    </div>
    <!--Main Layout-->
</div>
<?php $this->end();?>
