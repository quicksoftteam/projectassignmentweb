<?php
use App\Core\Dom;
use App\Core\FH;
use App\Core\Router;
$menu = [];//Router::getMenu('menu_acl');
$currentPage = 1;//FH::currentPage();
?>

<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark bg-primary lighten-1 pl-lg-5 pr-lg-5">
  <a class="navbar-brand" href="<?=PROJECT_PATH?>"><?=DEFAULT_BRAND?></a>
  <?=Dom::button(
            [ //attribute of button
              'class' => 'navbar-toggler',
              'dataset' => ['toggle' => 'collapse', 'target' => '#main-menu'],
              'aria' => ['controls' =>'main-menu', 'expanded' => 'false', 'label' => 'Toggle navigation']
            ], 
            // in side of button contain span
            Dom::span(['class' => 'navbar-toggler-icon'])
          );
  ?>
  <div class="collapse navbar-collapse" id="main-menu">
    <ul class="navbar-nav mr-auto">
      <?php foreach ($menu as $key => $value) : ?>
        <?php if (is_array($value)): ?>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?=$key?>
            </a>
            <div class="dropdown-menu dropdown-secondary" aria-labelledby="navbarDropdownMenuLink">
              <?php foreach ($value as $k => $v) : ?>
                <a class="dropdown-item" href="<?=$v?>"><?=$k?></a>
              <?php endforeach; ?>
            </div>
          </li>

        <?php else: ?>

          <?php $active = $currentPage == $value? 'active' : '';?>

          <?php if (preg_match('/register/', $value)) continue;?>
          <li class="nav-item <?=$active?>">
            <a class="nav-link" href="<?=$value?>"><?=$key?>
              <span class="sr-only">(current)</span>
            </a>
          </li>

        <?php endif; ?>

      <?php endforeach; ?>
    </ul>


      <?php $user = FH::currentUser();?>
    <ul class="navbar-nav ml-auto nav-flex-icons">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <img src="<?=PROJECT_PATH.FH::userProfileImage()?>" id="img" class="rounded-circle img-responsive" style="width: 25px; height: 25px"></i>
                <?php if ($user):?>
                    <span class="clearfix d-none d-sm-inline-block h6 pr-lg-1"><?=$user->fname . ' ' . $user->lname?></span>
                <?php endif;?>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <?php if (!$user):?>
                    <a class="dropdown-item" href="<?=PROJECT_PATH?>register/login">ចូលទៅកាន់គណនី</a>
                    <a class="dropdown-item" href="<?=PROJECT_PATH?>register/signup">ចុះឈ្មោះ</a>
                <?php else:?>
                    <a class="dropdown-item" href="<?=PROJECT_PATH?>register/logout">ចាកចេញ</a>
                <?php endif;?>
            </div>
        </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->