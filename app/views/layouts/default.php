<?php

use App\Core\FH;
use App\Core\Session;
use App\Models\Post;
use app\models\Settings;

$setting = Session::get('settings') ?? $this->setting ?? false;
if (!$setting)
{
    $setting_model = new Settings();
    $setting = $setting_model->findFirst();
    $setting = objectToArray($setting ?? $setting_model);
}

$posts = Session::get('posts') ?? $this->posts ?? false;
if (!$posts)
{
    $post_model = new Post();
    $posts = $post_model->findWithCategory(5);
    $posts = objectToArray($posts ?? $post_model);
}

?>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?=$this->_siteTitle;?></title>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
	<!-- Bootstrap core CSS -->
	<link href="<?=PROJECT_PATH?>public/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="<?=PROJECT_PATH?>public/css/mdb.min.css" rel="stylesheet">
	<!-- Your custom styles (optional) -->
	<link href="<?=PROJECT_PATH?>public/css/style.css" rel="stylesheet">

	<?=$this->content('head'); ?>
</head>

<body>
<?php include 'main_menu.php'; ?>
<div class="container-fluid">
	<?=$this->content('body'); ?>
</div>

<!-- Footer -->
<footer class="page-footer font-small unique-color-dark">

    <div class="bg-primary darken-3">
        <div class="container">

            <!-- Grid row-->
            <div class="row py-4 d-flex align-items-center">

                <!-- Grid column -->
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                    <h6 class="mb-0">Get connected with us on social networks!</h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->

                <!-- Grid column -->

            </div>
            <!-- Grid row-->

        </div>
    </div>

    <!-- Footer Links -->
    <div class="container-fluid text-center">

        <!-- Grid row -->
        <div class="row mt-3">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">តំណភ្ជាប់</h6>
                <hr class="brown darken-3 accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <?=FH::generateLinkForFooter(array_slice(objectToArray($posts), 0, 5, true))?>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">សមាជិកក្រុម</h6>
                <hr class="brown darken-3 accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <?=FH::generateMemberForFooter($setting['member'])?>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                <!-- Content -->
                <h6 class="text-uppercase font-weight-bold">អំពីយើង</h6>
                <hr class="brown darken-3 accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p class="text-left" style="overflow-wrap: break-word;"><?=$setting['about_us']?></p>


            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">ទំនាក់ទំនង</h6>
                <hr class="brown darken-3 accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p><i class="fas fa-home mr-3"></i><?=$setting['address']?></p>
                <p><i class="fas fa-envelope mr-3"></i><?=$setting['email']?></p>
                <?php if ($setting['phone1']):?>
                    <p><i class="fas fa-phone mr-3"></i><?=$setting['phone1']?></p>
                <?php endif;?>
                <?php if ($setting['phone2']):?>
                    <p><i class="fas fa-phone mr-3"></i><?=$setting['phone2']?></p>
                <?php endif;?>
                <?php if ($setting['phone3']):?>
                <p><i class="fas fa-phone mr-3"></i><?=$setting['phone3']?></p>
                <?php endif;?>
            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© <?=date('Y')?> Copyright:
        <a href="<?=PROJECT_PATH?>"><?=$setting['site_title']?></a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->
<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/mdb.min.js"></script>

<!-- custom script -->
<script type="text/javascript">let project_path = '<?=PROJECT_PATH?>';</script>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/script.js"></script>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/loadingoverlay.min.js"></script>
<?=$this->content('script');?>
<script>
    $(window).on('beforeunload', function(){
        return $.showSpinner();
    });

    $(document).ready(function () {
        $.hideSpinner();
    })
</script>
</body>

</html>