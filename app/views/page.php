<?php $this->setSiteTitle($this->title);

use App\Core\H; ?>

<?php $this->start('body');?>
    <div class="container-fluid">
        <?=H::stringToHTML($this->body)?>
    </div>
<?php $this->end();?>
