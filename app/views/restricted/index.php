<?php $this->setSiteTitle('404 Not Found.')?>

<?php $this->start('head') ?>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">
<?php $this->end() ?>

<?php $this->start('body') ?>
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h1>Oops!</h1>
                <h2>404 - The Page can't be found</h2>
            </div>
            <a href="<?=PROJECT_PATH?>home">Go TO Homepage</a>
        </div>
    </div>
<?php $this->end() ?>

<?php $this->start('script') ?>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<?php $this->end() ?>


