<?php use App\Core\Dom;
use App\Core\FH;

$this->start('head'); ?>

<?php $this->end(); ?>

<?php $this->start('body'); ?>

<div class="container my-5 py-5 z-depth-1">


	<!--Section: Content-->
	<section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">


		<!--Grid row-->
		<div class="row d-flex justify-content-center">

			<!--Grid column-->
			<div class="col-md-9 col-sm-9 col-lg-6">

				<!-- Default form register -->
				<form method="post" action="<?=PROJECT_PATH;?>register/signup" class="needs-validation" novalidate>
					<?=Dom::p(['class'=>'h4 mb-4'],'Sign up')?>
					<?=FH::displayErrors($this->displayErrors)?>
					<?= Dom::div(['class'=> "form-row"],
							Dom::div( ['class'=>'col'],
								FH::inputBlock('text','','fname',$this->newUser->fname,['class'=>'form-control', 'placeholder'=>'First name'],['class'=>'md-form mt-0'])
							),
							Dom::div( ['class'=>'col'],
								FH::inputBlock('text','','lname',$this->newUser->lname,['class'=>'form-control', 'placeholder'=>'Last name'],['class'=>'md-form mt-0'])
							)
						)
					?>

					<!-- Username -->
					<?= FH::inputBlock('text','','username',$this->newUser->username,['class'=>'form-control', 'placeholder'=>'Username'],['class'=>'md-form mt-0'])?>

					<!-- E-mail -->
					<?= FH::inputBlock('text','','email',$this->newUser->email,['class'=>'form-control', 'placeholder'=>'E-mail or Phone'],['class'=>'md-form mt-0'])?>

					<!-- Password -->
					<?= FH::inputBlock('password','','password',$this->newUser->password,['class'=>'form-control', 'placeholder'=>'Password'],['class'=>'md-form mt-0'])?>

					<!-- Confirm Password -->
					<?= FH::inputBlock('password','','confirm-password',$this->newUser->getConfirm(),['class'=>'form-control', 'placeholder'=>'Confirm Password'],['class'=>'md-form mt-0'])?>

					<!-- Sign up button -->
					<?=Dom::button(['class'=>'btn btn-info my-4 btn-block', 'type'=>'submit'], 'Sign up')?>

					<!-- Social register -->
					<?=Dom::p([], 'or sign up with:')?>

					<?=Dom::a(['href='=>'#','class'=>'mx-1', 'role'=>'button'], Dom::i(['class' => 'fab fa-facebook-f']))?>
					<?=Dom::a(['href='=>'#','class'=>'mx-1', 'role'=>'button'], Dom::i(['class' => 'fab fa-twitter']))?>
					<?=Dom::a(['href='=>'#','class'=>'mx-1', 'role'=>'button'], Dom::i(['class' => 'fab fa-linkedin-in']))?>
					<?=Dom::a(['href='=>'#','class'=>'mx-1', 'role'=>'button'], Dom::i(['class' => 'fab fa-github']))?>

					<hr>

					<!-- Terms of service -->
					<p>By clicking
						<em>Sign up</em> you agree to our
						<a href="<?=PROJECT_PATH;?>privacy/term" target="_blank">terms of service</a>

				</form>
				<!-- Default form register -->

			</div>
			<!--Grid column-->

		</div>
		<!--Grid row-->


	</section>
	<!--Section: Content-->


</div>
<?php $this->end(); ?>

<?php $this->start('script')?>
<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
            const forms = document.getElementsByClassName('needs-validation');
            console.log(forms);
            // Loop over them and prevent submission
            const validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
<?php $this->end()?>
