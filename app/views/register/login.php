<?php $this->start('head');

use App\Core\FH; ?>

<?php $this->end(); ?>

<?php $this->start('body'); ?>

<div class="container my-5 py-5 z-depth-1">


	<!--Section: Content-->
	<section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">


		<!--Grid row-->
		<div class="row d-flex justify-content-center">

			<!--Grid column-->
			<div class="col-md-6">

				<!-- Default form login -->
				<form class="text-center" method="post" action="<?=PROJECT_PATH;?>register/login">
					<div><?=FH::displayErrors($this->displayErrors)?></div>
					<p class="h4 mb-4">Sign in</p>

					<!-- Email -->
					<input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail">

					<!-- Password -->
					<input type="password" id="password" name="password" class="form-control mb-4" placeholder="Password">

					<div class="d-flex justify-content-around">
						<div>
							<!-- Remember me -->
							<?= FH::checkboxBlock('Remember me', 'remember_me', $this->login->getRememberMeChecked(), ['class' => 'custom-control-input'], ['class'=>'custom-control custom-checkbox'], ['class' => 'custom-control-label'])?>
						</div>
						<div>
							<!-- Forgot password -->
							<a href="">Forgot password?</a>
						</div>
					</div>

					<!-- Sign in button -->
					<button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

					<!-- Register -->
					<p>Not a member?
						<a href="<?=PROJECT_PATH?>register/signup">Register</a>
					</p>

					<!-- Social login -->
					<p>or sign in with:</p>

					<a href="#" class="mx-1" role="button"><i class="fab fa-facebook-f"></i></a>
					<a href="#" class="mx-1" role="button"><i class="fab fa-twitter"></i></a>
					<a href="#" class="mx-1" role="button"><i class="fab fa-linkedin-in"></i></a>
					<a href="#" class="mx-1" role="button"><i class="fab fa-github"></i></a>

				</form>
				<!-- Default form login -->

			</div>
			<!--Grid column-->

		</div>
		<!--Grid row-->


	</section>
	<!--Section: Content-->


</div>

<?php $this->end(); ?>
