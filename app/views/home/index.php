<?php $this->setSiteTitle('Home');

use App\Core\H; ?>

<?php $this->start('head'); ?>
<link href="<?=PROJECT_PATH?>public/css/addons/datatables.min.css" rel="stylesheet">
<?php $this->end(); ?>

<?php $this->start('body'); ?>
<!-- main carousel -->
<div class="container ">
    <div class="row">
        <!--Carousel Wrapper-->
        <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!--Indicators-->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-2" data-slide-to="1"></li>
                <li data-target="#carousel-example-2" data-slide-to="2"></li>
            </ol>
            <!--/.Indicators-->
            <!--Slides-->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="view">
                        <img class="d-block w-100 fill" src="<?=PROJECT_PATH?>public/img/carousel/1.jpg"
                             alt="First slide">
                        <div class="mask rgba-black-light"></div>
                    </div>
                    <div class="carousel-caption">
                        <h3 class="h3-responsive">Temple</h3>
                        <p>Angkor Wat</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <!--Mask color-->
                    <div class="view">
                        <img class="d-block w-100 fill" src="<?=PROJECT_PATH?>public/img/carousel/2.jpg"
                             alt="Second slide">
                        <div class="mask rgba-black-strong"></div>
                    </div>
                    <div class="carousel-caption">
                        <h3 class="h3-responsive">Beach</h3>
                        <p>Otres</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <!--Mask color-->
                    <div class="view">
                        <img class="d-block w-100 fill" src="<?=PROJECT_PATH?>public/img/carousel/4.jpg"
                             alt="Third slide">
                        <div class="mask rgba-black-slight"></div>
                    </div>
                    <div class="carousel-caption">
                        <h3 class="h3-responsive">Mountian</h3>
                        <p>Bokor resort</p>
                    </div>
                </div>
            </div>
            <!--/.Slides-->
            <!--Controls-->
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <!--/.Controls-->
        </div>
        <!--/.Carousel Wrapper-->
    </div>
    <!-- Card -->
    <div class="card card-cascade wider reverse">


        <!-- Card content -->
        <div class="card-body card-body-cascade text-center">

            <!-- Title -->
            <h2 class="font-weight-bold"><a>សូមស្វាគមន៍</a></h2>
            <!-- Data -->
            <p><a><strong>មកកាន់គេហទំព័រ សុវណ្ណភូមិ</strong></a></p>
            <!-- Social shares -->
            <div class="social-counters">
                <!-- Facebook -->
                <a class="btn btn-fb">
                    <i class="fab fa-facebook-f pr-2"></i>
                    <span class="clearfix d-none d-md-inline-block">Facebook</span>
                </a>
                <!-- <span class="counter">46</span> -->
                <!-- Twitter -->
                <a class="btn btn-tw">
                    <i class="fab fa-twitter pr-2"></i>
                    <span class="clearfix d-none d-md-inline-block">Twitter</span>
                </a>
                <!-- <span class="counter">22</span> -->
                <!-- Google+ -->
                <a class="btn btn-gplus">
                    <i class="fab fa-google-plus-g pr-2"></i>
                    <span class="clearfix d-none d-md-inline-block">Google+</span>
                </a>
                <!-- <span class="counter">31</span> -->
                <!-- Comments -->
                <a class="btn btn-default">
                    <i class="far fa-comments pr-2"></i>
                    <span class="clearfix d-none d-md-inline-block">Comments</span>
                </a>
                <!-- <span class="counter">18</span> -->
            </div>
            <!-- Social shares -->

        </div>
        <!-- Card content -->

    </div>
    <!-- Card -->
</div>
<!-- main carousel -->

<div class="container mt-5">

    <section>
        <?php foreach ($this->categories as $category):?>
            <!--Grid column-->
            <div class="row">
                <dic class="col-12">
                    <h2 class="news-tile mb-4 font-weight-bold"><?=$category->title?></h2>
                </dic>
                <?php $limit = 0;?>
                <?php foreach ($this->posts as $post):?>
                    <?php if($post['category_id'] == $category->id):?>
                        <?php $limit++;?>
                        <?php if ($limit> 8) continue;?>
                        <!--Grid column-->
                        <div class="col-sm-6 col-lg-3 col-md-6 col-12 mb-3">
                            <!--News tile -->
                            <div class="news-tile view zoom z-depth-1 rounded mb-4">
                                <a href="<?=H::resolvePostDetailUrl($post['category_title'], $post['id'])?>" class="text-white">
                                    <img src="<?=H::resolveImageUrl($post['image_feature']) ?? H::defaultImage('ic_compose.png')?>"
                                         class="img-fluid rounded-bottom" alt="<?="Image of {$post['post_title']}"?>">
                                    <div class="mask rgba-stylish-strong">
                                        <div class="text-white text-center py-lg-5 py-0 my-0">
                                            <div>
                                                <h4 class="card-title font-weight-bold pt-2">
                                                    <strong><?=H::resolveContentLength($post['post_title'], 14)?></strong>
                                                </h4>
                                                <p class="mx-5 clearfix d-none d-md-block"></p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <!--News tile -->
                        </div>
                        <!--Grid column-->
                    <?php endif;?>
                <?php endforeach;?>
            </div>
            <hr>
            <!--Grid column-->
        <?php endforeach;?>
    </section>


</div>


<?php $this->end(); ?>

<?php $this->start('script')?>
<script type="text/javascript" src="<?=PROJECT_PATH?>public/js/addons/datatables.min.js"></script>
<script>
$(document).ready(function () {
$('#dtOrderExample').DataTable({
    "order": [[ 3, "desc" ]]
});
$('.dataTables_length').addClass('bs-select');

});
</script>
<?php $this->end(); ?>


