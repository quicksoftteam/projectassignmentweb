<?php

/*
function dd($data, $die = true)
{
	echo '<pre>';
	print_r($data);
	echo '</pre>';
	if ($die) {
		die();
	}
}




function secure_js($value) {
    $js_entity = [
        '`'  => '\u0060',
        '~'  => '\u007E',
        '!'  => '\u0021',
        '@'  => '\u0040',
        '#'  => '\u0023',
        '$'  => '\u0024',
        '%'  => '\u0025',
        '^'  => '\u005E',
        '&'  => '\u0026',
        '*'  => '\u002A',
        '('  => '\u0028',
        ')'  => '\u0029',
        '-'  => '\u002D',
        '='  => '\u003D',
        '\\' => '\u005C',
        '+'  => '\u002B',
        '|'  => '\u007C',
        '['  => '\u005B',
        ']'  => '\u005D',
        '{'  => '\u007B',
        '}'  => '\u007D',
        ':'  => '\u003A',
        ';'  => '\u003B',
        '\'' => '\u0027',
        '"'  => '\u0022',
        ','  => '\u002C',
        '.'  => '\u002E',
        '/'  => '\u002F',
        '<'  => '\u003C',
        '>'  => '\u003E',
        '?'  => '\u003F',
        ' '  => '\u0020',
        "\n" => '\u000A',
        "\r" => '\u000D',
        "\t" => '\u0009',
        "\v" => '\u000B',
        "\e" => '\u001B',
        "\f" => '\u000C'];
    $return = '';
    $n = mb_strlen($value);
    for ($i = 0; $i<$n; $i++) {
        $c = mb_substr($value, $i, 1);
        if (array_key_exists($c, $js_entity)) {
            $c = $js_entity[$c];
        }
        $return .= $c;
    }

    return $return;
}

$str = 'age=12, name=dara, sex=male';
[
	'age' => 12,
	'name' => 'dara',
	'sex' => 'male'
]

function toArray($str, $seps = [])
{
	return array_reduce(explode($seps['separator'] ?? ',', $str), function($in, $item) use ($seps){
		[$key] = $items = explode($seps['item_separator'] ?? '=', trim($item));
		$in[$key] = $items[1] ?? $key;
		return $in;
	}, [])
}

function array_fold($array, $cb, $init)
{
	$keys = array_keys($array);
		return array_reduce($keys, function ($cb_init, $key) use ($array, $cb) {
			return $cb($cb_init, $key, $array[$key]);
	}, $init);
}


function toArray($str, $seps = [])
{
	return array_reduce(explode($seps['separator'] ?? ',', $str), function($in, $item) use ($seps){
		[$key] = $items = explode($seps['item_separator'] ?? '=', trim($item));
		//$in[$key] = $items[1] ?? $key; // another way
		($items[1] ?? false) ? 
		$in[$key] = $items[1] :
		$in[] = $key;
		return $in;
	}, []);
}

$str = 'age:12; name:dara; sex:male; location:Phnom Penh';
print_r(toArray($str, ['separator' => ';', 'item_separator' => ':']));

*/

use App\Core\Dom;

function getMenuTree($parent_id)
{
    $menu = "";
    $db = App\Core\Database::getInstance();
    $res = $db->find("menu",[
        'conditions' => ['status'=>'?', 'parent_id'=>'?'],
        'bind'=>[1, $parent_id]
    ]);
    if ($res)
    {
        foreach (objectToArray($res) as $value){
            $sub_menu = getMenuTree($value['id']);
            if (strlen($sub_menu) != 0)
            {
                $menu .= '<li class="dropdown dropdown-submenu navbar-nav">';
                $menu .= Dom::button(
                    [
                        'class' => 'btn btn-primary dropdown-toggle btn-sm',
                        'type'=>'button',
                        'id' => 'dropdownMenu2',
                        'dataset'=>['toggle'=>'dropdown'],
                        'aria' =>['haspopup'=>'true', 'expanded'=>'false']
                    ],
                    $value['name']
                );
            }
            else
            {
                $menu .= '<li class="navbar-nav">';
                $menu .= Dom::a(['href' => $value['link'], 'class'=>'dropdown-item'], $value['name']);
            }
            if (strlen($sub_menu) != 0) $menu .= '<ul class="dropdown-menu multi-level-dropdown" role="menu" aria-labelledby="dropdownMenu">'.$sub_menu.'</ul>';
            $menu .= "</li>";
        }
    }
    return $menu;
}

function objectToArray($data) {

    if (is_object($data)) {
        $data = get_object_vars($data);
    }

    if (is_array($data)) {
        return array_map(__FUNCTION__, $data);
    }
    else {
        return $data;
    }
}

class Log
{
    public static function info($message)
    {
        $log_info_file = fopen(FILE_APP_LOG_FILE, 'w+');
        fwrite($log_info_file, $message);
        fclose($log_info_file);
    }

    public static function error($message)
    {
        $log_error_file = fopen(FILE_ERROR_LOG_FILE, 'w+');
        fwrite($log_error_file, $message);
        fclose($log_error_file);
    }
}