<?php
require_once(ROOT . DS .'app'. DS .'lib'. DS .'helpers'. DS .'helpers.php');

function debugToConsole($value)
{
    $output = $value;
    if (is_array($output))
        $output = implode(',', $output);
    elseif (is_object($output))
        $output = print_r($output, true);
    echo "<script>console.log(`Debug Objects: " . $output . "`);</script>";
}