<?php

namespace App\Core;
use App\Core\Dom;
use App\Core\Session;
use app\models\Roles;
use App\Models\User;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use stdClass;

/**
 * Helper class contain all functions to help generate HTML Forms
 * all functions return string of the Dom Class
 * @Author Ratana
 * @Date: 1/1/2020 3:30 pm
 */
class FH
{

	public static function inputBlock($type, $label, $name, $value = '' ,$input_attr = [], $div_attr = [], $label_attr = [])
	{
		return Dom::div(
			$div_attr , 
			Dom::label(array_merge(['for' => $name], $label_attr), $label),
			Dom::input(array_merge(['type' => $type, 'id' => $name, 'name' => $name, 'value' => $value], $input_attr))
		);
	}

	public static function submitTag($button_text, $input_attr = [])
	{
		return Dom::input(array_merge(['type' => 'submit', 'value' => $button_text], $input_attr));
	}

	public static function submitBlock($button_text, $input_attr = [], $div_attr = [])
	{
		return Dom::div($div_attr, self::submitTag($button_text, $input_attr));
	}

	public static function checkBoxBlock($label, $name, $check = false, $input_attr = [], $div_attr = [], $label_attr = [])
	{
		if ($check) $input_attr = array_merge(['checked'=> 'checked'], $input_attr);
		return Dom::div(
			$div_attr,
			Dom::input(array_merge(['type' => 'checkbox', 'id' => $name, 'name'=> $name], $input_attr)),
			Dom::label(array_merge(['for' => $name], $label_attr), $label)
		);
	}

	public static function generateToken()
	{
		$token = base64_encode(openssl_random_pseudo_bytes(32));
		Session::set('csrf_token', $token);
		return $token;
	}

	public static function getBaseUrl($url)
	{
		return PROJECT_PATH .${$url};
	}

	public static function checkToken($token)
	{
		return (Session::exist('csrf_token') && Session::get('csrf_token') == $token);
	}

	public static function tokenInput()
	{
		return Dom::input(['type' => 'hidden', 'id' => 'csrf_token', 'name' => 'csrf_token', 'value' => self::generateToken()]);
	}

	public static function currentUser()
	{
		return User::currentLoggedInUser();
	}

	public static function postValue($post)
	{
		return array_reduce(array_keys($post), function($init, $key) use ($post){
			$init[$key] = Input::sanitize($post[$key]);
			return $init;
		}, []);
	}

	public static function currentPage()
	{
		$currentPage = $_SERVER['REQUEST_URI'];
		if ($currentPage == PROJECT_PATH || $currentPage == PROJECT_PATH .'home/index') {
			$currentPage = PROJECT_PATH . 'home';
		}
		return $currentPage;
	}

	public static function getObjectProperties($object, $isObject = false)
	{
        try {
            $default_object = new stdClass();
            $default_arrays = [];

            $_object = new ReflectionClass($object);
            $is_public = $_object->getProperties(ReflectionProperty::IS_PUBLIC);
            $is_static = $_object->getProperties(ReflectionProperty::IS_STATIC);
            $filter = array_diff($is_public, $is_static);
            foreach ($filter as $property)
            {
                $name = $property->name;
                if ($property->class == $_object->name)
                {
                    if ($isObject)
                        $default_object->$name = $object->$name;
                    else
                        $default_arrays[$name] = $object->$name;
                }
            }
            return $isObject? $default_object : $default_arrays;
        } catch (ReflectionException $e) {
            return null;
        }
	}

	public static function displayErrors($errors)
	{
		$html = '<div><ul>';
		$html .= array_reduce(array_keys($errors), function ($html, $field) use ($errors){
			$html .= '<li class="text-danger">'.$errors[$field].'</li>';
			$html .= '<script>window.onload = function() { $("#'.$field.'").parent().closest("div").addClass("is-invalid");}</script>';
			return $html;
		}, '');
		$html .= '</ul></div>';
		return $html;
	}

	public static function isAuthorize($role)
    {
        $roles = Session::get('UserRole');
        $user = FH::currentUser();
        if (!$roles && $user->user_type == SUPER_ADMIN_TYPE) { //because admin type is always checking in route so no need to check otherwise is admin because admin is only one check
            $roles = (new Roles())->find(['conditions'=> 'user_id = ?', 'bind'=>[$user->id]]);
            $roles = $roles? objectToArray($roles[0]): [];
            Session::set('UserRole', objectToArray($roles));
        }
        if (is_array($role))
        {
            foreach ($role as $k)
            {
                if ($roles[$k] == 1) return true;
            }
            return false;
        }
        else
            if ($user->user_type == SUPER_ADMIN_TYPE OR $roles[$role] == 1) return true;
        return false;
    }

    public static function generateMemberForFooter($json)
    {
        $data = json_decode($json, true);
        return Dom::fragment(array_reduce($data, function ($in, $v){
            $in[] = Dom::p([], Dom::a(['href'=> $v['href']],$v['name']));
            return $in;
        }, []));
    }

    public static function generateLinkForFooter($post)
    {
        return Dom::fragment(array_reduce($post, function ($in, $v){
            $in[] = Dom::p([], Dom::a(['href'=> H::resolvePostDetailUrl($v['category_title'], $v['id'])],$v['post_title']));
            return $in;
        }, []));
    }

    public static function userProfileImage()
    {
        $user = self::currentUser();
        if (!$user)
        {
            return H::defaultImage('user_profile_white.png');
        }
        return H::defaultImage($user->photo);
    }

}