<?php


namespace app\core;


class StatusCode
{
    public static $internalError = 500;
    public static $errorNotFound = 404;
    public static $errorNotAuthorize = 401;
    public static $success = 200;
}