<?php

namespace App\Core;
use App\Core\Database;
use App\Core\FH;
use stdClass;

class Model
{
    protected $_db;
	protected $_table;
	protected $_modelName;
	protected $_softDelete = false;
	protected $_validates = true;
	protected $_validationErrors = [];

	public function __construct($table)
	{
		$this->_db = Database::getInstance();
		$this->_table = $table;
		$this->_modelName = str_replace(' ', '', ucwords(str_replace('_', ' ', $this->_table)));
	}


	public function getCoulumns()
	{
		return $this->_db->getColumns($this->_table);
	}

	protected function _softDeleteParams($params = [])
	{
		if ($this->_softDelete) {
			if (array_key_exists('conditions', $params)) {
				if (is_array($params['conditions'])) {
					$params['conditions'][]= " deleted != 1";
				}
				else
				{
					$params['conditions'] .= " AND deleted != 1";
				}
			}
			else
			{
				$params['conditions'] = " deleted != 1";
			}
		}
		return $params;
	}

	public function find($params = [])
	{
		$params = $this->_softDeleteParams($params);
		return $this->_db->find($this->_table, $params, get_class($this));
	}

	public function findFirst($params = [])
	{
		$params = $this->_softDeleteParams($params);
		return $this->_db->findFirst($this->_table, $params, get_class($this));
	}

	public function findById($id)
	{
		return $this->findFirst(['conditions' => 'id = ?', 'bind' => [$id]]);
	}

	public function insert($fields)
	{
		if (empty($fields)) return false;
		return $this->_db->insert($this->_table, $fields);
	}

	public function update($id, $fields)
	{
		if (empty($fields)) return false;
		return $this->_db->update($this->_table, $id ,$fields);
	}

	public function delete($id = '')
	{
		if ($id == '' && $this->id == '') return false;
		$id = ($id == '') ? $this->id : $id;
		if ($this->_softDelete)
		{
			return $this->update($id, ['deleted' => 1]);
		}
		return $this->_db->delete($this->_table, $id);
	}

	public function save()
	{
		$this->validator();
		if ($this->_validates) 
		{
			$this->beforeSave();
			$fields = FH::getObjectProperties($this);
			// determine whether to  update or insert
			if (property_exists($this, 'id') && $this->id != '')
			{
				$save = $this->update($this->id, $fields);
				$this->afterSave();
				return $save;
			}
			$save = $this->insert($fields);
			$this->afterSave();
			return $save;
		}
		return false;
	}

	public function query($sql, $bind = [])
	{
		return $this->_db->query($sql, $bind);
	}

	// generate array from database to object
	protected function populateObject($resuls)
	{
		foreach ($resuls as $key => $value)
		{
			$this->$key = $value;
		}
	}

	public function data()
	{
		$data = new $this();
		foreach (FH::getObjectProperties($this) as $column => $value)
		{
			$data->column = $value;
		}
		return $data;
	}

	// Assing New Value that Not NULL
	public function assign_field($params)
	{
		$arr = [];		
	    foreach ($params as $key => $value)
        {
			if(property_exists($this, $key))
			{
				$arr[$key] = $value;
			}
		}
		return $arr;
	}

	public function assign($params, $list = [], $blackList = true)
	{
	    foreach ($params as $key => $value)
        {
            $whiteListed = true;
            if (count($list) > 0)
            {
                if ($blackList)
                {
                    $whiteListed = !in_array($key, $list);
                }
                else
                {
                    $whiteListed = in_array($key, $list);
                }
            }
            if (property_exists($this, $key) && $whiteListed)
            {
                $this->$key = $value;
            }
            
        }
		return $this;
	}

	public function validator(){}

	public function runValidation($validator)
	{
		$key = $validator->field;
		if (!$validator->success)
		{
			$this->_validates = false;
			$this->_validationErrors[$key] = $validator->msg;
		}
	}

	public function getErrorMessages()
	{
		return $this->_validationErrors;
	}

	public function validationPassed()
	{
		return $this->_validates;
	}

	public function addErrorMessage($field, $msg)
	{
		$this->_validates = false;
		$this->_validationErrors[$field] = $msg;
	}

	public function beforeSave(){}
	public function afterSave(){}

	public function isNew()
	{
		return (property_exists($this, 'id') && !empty($this->id))? false : true;
	}
}