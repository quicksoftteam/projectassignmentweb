<?php

namespace App\Core;

class Security
{
	private static $_key = 'h3zTslUG3QyIZpbDbW2r46KrVWqj7NMTn';
	private static $_cipher = 'aes-128-gcm';
	private static $_options = 0;
	private static $_tag;
	private static $_iv = 128;

	public static function encrypt($text)
	{
		if (in_array(self::$_cipher, openssl_get_cipher_methods()))
		{
			return openssl_encrypt(base64_encode($text), self::$_cipher, self::$_key, self::$_options=0, self::$_iv, self::$_tag);
		}
		return $text;
	}

	public static function decrypt($text)
	{
		if (in_array(self::$_cipher, openssl_get_cipher_methods()))
		{
			return base64_decode(openssl_decrypt($text, self::$_cipher, self::$_key, self::$_options=0, self::$_iv, self::$_tag));
		}
		return $text;
	}

}