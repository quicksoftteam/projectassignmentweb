<?php

namespace App\Core;
use App\Core\H;
use PDO;
use PDOException;

class Database
{
	private static $_instance = null;
	private $_pdo;
	private $_query;
	private $_error = false;
	private $_result;
	private $_count = 0;
	private $_lastInsertedId = null;
	private $_errorInfo = null;


	private function __construct()
	{
		try
		{
			$this->_pdo = new PDO("mysql:charset=utf8mb4;host=".DB_HOST.";dbname=".DB_NAME , DB_USERNAME, DB_PASSWORD);
			if (DEBUG) 
			{
				$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->_pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			}
		}
		catch (PDOException $exception)
		{
			H::dd($exception->getMessage());
		}
	}

	public static function getInstance()
	{
		if (!isset(self::$_instance))
		{
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function result()
	{
		return $this->_result;
	}

	public function error()
	{
		return $this->_error;
	}

	public function query($sql, $params = [], $class = false)
	{
		$this->_error = false;
		if ($this->_query = $this->_pdo->prepare($sql))
		{
			$x = 1;
			if (count($params))
			{
				foreach ($params as $param)
				{
					$this->_query->bindValue($x, $param);
					$x++;
				}
			}
			if ($this->_query->execute())
			{
				$this->_lastInsertedId = $this->_pdo->lastInsertId();
				if ($class) 
				{
					$this->_result = $this->_query->fetchAll(PDO::FETCH_CLASS,$class);
				}
				else
				{
					$this->_result = $this->_query->fetchAll(PDO::FETCH_OBJ);
				}
				$this->_count = $this->_query->rowCount();
			}
			else
			{
				$this->_errorInfo = $this->_query->errorInfo();
				$this->_error = true;
			}
		}
		return $this;
	}

	public function insert($table, $fields = [])
	{
		$field_string = '';
		$value_string = '';
		$values = [];
		foreach ($fields as $field => $value)
		{
			$field_string .= '`' .$field . '`,';
			$value_string .= '?,';
			$values[] = $value;
		}
		$field_string = rtrim($field_string, ',');
		$value_string = rtrim($value_string, ',');
		$sql = "INSERT INTO {$table} ({$field_string}) VALUES ({$value_string})";
		if (!$this->query($sql, $values)->error())
		{
			return true;
		}
		return false;
	}

    /** @noinspection SqlResolve */
    public function update($table, $id, $fields = [])
	{
		$field_string = '';
		$values = [];

		foreach ($fields as $field => $value)
		{
		    if ($field != 'id')
            {
                $field_string .= ' ' .$field. ' = ?,';
                $values[] = $value;
            }
		}

		$values[] = $id;
		$field_string = rtrim(trim($field_string), ',');
		$sql = "UPDATE {$table} SET {$field_string} WHERE id = ?";

		if (!$this->query($sql, $values)->error())
		{
			return true;
		}
		return false;
	}

    /** @noinspection SqlResolve */
    public function delete($table, $id)
	{
		$sql = "DELETE FROM {$table} WHERE id = ?";
		if (!$this->query($sql, [$id])->error())
		{
			return true;
		}
		return false;
	}

	protected function _read($table, $params = [], $class = false)
	{
		$condition_string = '';
		$bind = [];
		$orderBy = '';
		$limit = '';
		
		//conditions
		if (array_key_exists('conditions',$params))
		{
			if (is_array($params['conditions']))
			{
				foreach ($params['conditions'] as $condition => $value)
				{
					$condition_string .= ' '.$condition.'='.$value.' AND';
				}
			}
			else
			{
				$condition_string = $params['conditions'];
			}
			$condition_string = rtrim($condition_string, 'AND');
			if (!empty($condition_string))
			{
				$condition_string = ' WHERE '. $condition_string;
			}
		}
		
		//bind
		if (array_key_exists('bind', $params))
		{
			$bind = $params['bind'];
		}
		
		//order by
		if (array_key_exists('orderBy', $params))
		{
			$orderBy = ' ORDER BY ' .$params['orderBy'];
		}
		
		//limit
		if (array_key_exists('limit', $params))
		{
			$limit = ' LIMIT '.$params['limit'];
		}
		
		$sql = "SELECT * FROM {$table} {$condition_string}{$orderBy}{$limit}";
		if ($this->query($sql, $bind, $class))
		{
			if (!count($this->_result)) return false;
			return true;
		}
		return false;
		
	}

	public function find($table, $params = [], $class = false)
	{
		if ($this->_read($table, $params, $class))
		{
			return $this->result();
		}
		return false;
	}

	public function findFirst($table, $params = [], $class = false)
	{
		if ($this->_read($table, $params, $class))
		{
			return $this->first();
		}
		return false;
	}

	public function first()
	{
		return !empty($this->_result)? $this->_result[0] : [] ;
	}

	public function count()
	{
		return $this->_count;
	}

	public function lastId()
	{
		return $this->_lastInsertedId;
	}

	public function getColumns($table)
	{
		return $this->query("SHOW COLUMNS FROM {$table}")->result();
	}

	public function errorInfo()
	{
		return $this->_errorInfo;
	}
}