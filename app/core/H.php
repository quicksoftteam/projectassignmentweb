<?php

namespace App\Core;
use App\Core\Session;
use App\Core\Dom;
use App\Models\Post;
use App\Core\StringUtil;

/*
 * This class is the helper class that going to work for help
 * Contain static functions
 * @Author Ratana
 * @Date: 1/1/2020 3:28 pm
 * */
class H
{
	public static function dd($data, $die = true)
	{
		echo '<pre>';
		print_r($data);
		echo '</pre>';
		if ($die) {
			die();
		}
	}

	/**
	 * @param string $errorType  it can be primary, secondary, success, danger, warning, info, light, dark
	 * @param string $errorMessage the message will display
	 */
	public static function addMsg($errorType, $errorMessage)
	{
		$session_name = 'alert-' . $errorType;
		Session::set($session_name, $errorMessage);
	}

    /**
     * @param $errorType string it can be info, warning, success, error
     * @param $errorMessage array | string the message will be display
     */
    public static function addMsgToast($errorType, $errorMessage)
    {
        $error = '';
        if (is_array($errorMessage))
        {
            foreach ($errorMessage as $key => $value)
            {
                $error .= ucwords($key) . ': '.$value.'<br>';
            }
        }
        else
        {
            $error = $errorMessage;
        }
        Session::set($errorType, $error);
    }

	/**
	 * display alert 
	 * @return string error toast message
	 */
	public static function displayMsgToast()
	{
		$error_type = ['success', 'warning', 'info', 'error'];
		$html_toast = '';
		foreach ($error_type as $type)
		{
			if (Session::exist($type))
			{
                $html_toast = Dom::script("toastr.$type(`". Session::get($type)."`);");
				Session::delete($type);
			}
		}
		return $html_toast;
	}

    /**
     * display alert
     * @return string error alert message
     */
	public static function displayMsg()
    {
        $error_type = ['alert-primary', 'alert-secondary', 'alert-success', 'alert-danger', 'alert-warning', 'alert-info', 'alert-light', 'alert-dark'];
        $html = '';
        foreach ($error_type as $type)
        {
            if (Session::exist($type))
            {
                $html = Dom::div(
                    ['class' => "alert $type", 'role' => 'alert'],
                    Session::get($type),
                    Dom::button(
                        ['type'=>'button', 'class'=>'close', 'dataset'=>['dismiss'=>'alert'], 'aria'=>['label'=>'Close']],
                        Dom::span(['aria'=>['hidden'=>'true']], '×')
                    )
                );
                Session::delete($type);
            }
        }
        return $html;
    }

    /**
     * @param $category string
     * @param $id int
     * @return string
     */
	public static function resolvePostDetailUrl($category, $id)
    {
        return PROJECT_PATH . 'posts/'. self::encodeUrl($category) .'/detail/' .$id;
    }

    /**
     * @param $category
     * @return string
     */
    public static function resolvePostListUrl($category)
    {
        return PROJECT_PATH . 'posts/'. self::encodeUrl($category);
    }

    /**
     * @param $content string
     * @param int $length
     * @return string
     */
    public static function resolveContentLength($content, $length = 0)
    {
        $content  = strip_tags(html_entity_decode($content));

        return !$length? $content : StringUtil::substr($content, 0, $length) . (mb_strlen($content) <$length? '':' ...');
    }

    /**
     * @param $date string
     * @return false|string
     */
    public static function resolveFormatPostDate($date)
    {
        return date_format(date_create($date), 'd/m/Y');
    }

    /**
     * @param $image
     * @return string path of image that have been uploaded
     */
    public static function resolveImageUrl($image)
    {
        if (!$image) return null;
        return PROJECT_PATH .PATH_IMAGE_USERS_UPLOADED. $image;
    }

	/**
	 * @param $stringValue
	 * @return string real HTML
	 */
    public static function stringToHTML($stringValue)
	{
		return htmlspecialchars_decode($stringValue,ENT_COMPAT);
	}

    /**
     * @param $url
     * @return string
     */
    public static function encodeUrl($url)
    {
        $search_list = [' ', '\/', '\?', '\=', '&#039;'];
        $replace_list = ['-', '+', '@', '$', '_'];
        return urlencode(array_reduce(array_keys($search_list), function ($in , $k) use ($search_list, $replace_list){
            $in = preg_replace('/'.$search_list[$k].'/', $replace_list[$k], $in);
            return $in;
        }, $url));
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function decodeUrl($url)
    {
        $search_list = ['-', '\+', '@', '\$', '_'];
        $replace_list = [' ', '/', '?', '=', '&#039;'];
        return array_reduce(array_keys($search_list), function ($in , $k) use ($search_list, $replace_list){
            $in = preg_replace('/'.$search_list[$k].'/', $replace_list[$k], $in);
            return $in;
        }, $url);
    }

    public static function getBaseUrl($project_path = false, $additional = '')
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . ($project_path? PROJECT_PATH : ''). $additional;
    }

    public static function getUrl($additional = '')
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . $additional;
    }

    public static function getUrlLinkPostDetail($categoryTitle, $postId)
    {
        return self::getBaseUrl().self::resolvePostDetailUrl($categoryTitle, $postId);
    }

    public static function  getEngineName()
    {
        $isApache =  extension_loaded('apache2handler') ||(strpos(getenv('SERVER_SOFTWARE'),'Apache')!==false) ||  preg_match("/Apache/i", $_SERVER['SERVER_SOFTWARE']);
        $isNginx = preg_match("/Nginx/i", $_SERVER['SERVER_SOFTWARE']);
        return $isApache? APACHE_SERVER : ($isNginx? NGINX_SERVER : UNKNOWN_SERVER);
    }
    
    public static function isProduction()
	{
		return getenv('PRODUCTION') == 'true';
	}

    /**
     * @param $type string
     * @return string
     */
	public static function defaultImage($name)
	{
		return PATH_IMAGE_USERS_UPLOADED.$name;
	}
}