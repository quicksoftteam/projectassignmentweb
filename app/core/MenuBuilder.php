<?php

namespace App\Core;
use App\Core\Database;

class MenuBuilder
{
    private $_data;
    public function __construct()
    {
        $db = Database::getInstance();
        $this->_data = $db->find("menu",[
            'conditions' => ['status'=>'?'],
            'bind' => [1]
        ]);
    }

    public function objectToArray($data)
    {
        if (is_array($data) || is_object($data))
        {
            $result = array();
            foreach ($data as $key => $value)
            {
                $result[$key] = $this->objectToArray($value);
            }
            return $result;
        }
        return $data;
    }

    public function toJson()
    {
        return $this->_toJson($this->_data);
    }

    private function _toJson($data, $parenId = 0)
    {
        $menu = [];
        foreach ($data as $item) {
            if ($item->parent_id == 0) {
                $menu[] = $item;
            } else {
                if (!isset($data[$item->parent_id])) $data[$item->parent_id] = new \stdClass();
                $data[$item->parent_id]->children[] = $item;
            }
        }

        foreach ($data as $item) {
            unset($item->parent_id);
        }

        return json_encode($menu);
    }

    //@TODO convert json menu to dynamic menu

    //@TODO convert json menu to database menu
    public function toDatabaseMenu($json)
    {
        return $this->_getOrderData($this->objectToArray($json));
    }

    private function _getOrderData($data, $parentId = 0)
    {
        $last_value = [];
        foreach ($data as $val) {
            $new_data = [
                "id" => $val["id"],
                "name" => $val["name"],
                "link" => $val["link"],
                "status" => $val["status"],
                "parent_id" => $parentId
            ];
            $last_value[] = $new_data;
            if (isset($val["children"])) {
                $children = $this->_getOrderData($val["children"], $val["id"]);
                if ($children) {
                    $last_value = array_merge($last_value, $children);
                }
            }
        }
        return $last_value;
    }
}