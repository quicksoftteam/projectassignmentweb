<?php

namespace App\Core;
use app\controllers\DynamicPageController;
use app\controllers\DynamicPostController;
use App\Core\Session;
use App\Core\FH;
use App\Core\Dom;

class Router
{
	private static $controller;
	private static $admin_controller;
	private static $controller_name;
	private static $action;
	private static $action_name;
	private static $query_params;
	public static function route($url)
	{
		if ((isset($url[0]) && $url[0] != '') && strcmp($url[0], 'admin') == 0)
		{
			//controller
			self::$controller = (isset($url[1]) && $url[1] != '') ? ucwords($url[1]) . 'Controller' : DEFAULT_ADMIN_CONTROLLER;
			array_shift($url);
			//action
			self::$action = (isset($url[1]) && $url[1] != '' )? $url[1] .'Action' : 'indexAction';
			self::$action_name = (isset($url[1]) && $url[1] != '' )? $url[1] : 'index';
            array_shift($url);
			self::$admin_controller = 'admin';

			//grant access admin
            $grant_access = self::hasAccess(self::$controller, self::$action);
            if (!$grant_access)
            {
                self::redirect('restricted');
                exit();
            }
		}
		else
		{
			self::$admin_controller = '';
			self::$controller = (isset($url[0]) &&  !empty(trim($url[0]))) ? ucwords($url[0]) . 'Controller' : DEFAULT_CONTROLLER;
			array_shift($url);

			//action
			self::$action = (isset($url[0]) &&  !empty(trim($url[0])) )? $url[0] .'Action' : 'indexAction';
			self::$action_name = (isset($url[0]) &&  !empty(trim($url[0])) )? $url[0] : 'index';
		}
		self::$controller_name = preg_replace('/Controller/', '', self::$controller);
		array_shift($url);
		// query parameter
		self::$query_params = $url;


		$controller_namespace = 'App\Controllers\\';
		self::$controller = empty(self::$admin_controller)? $controller_namespace .self::$controller : $controller_namespace .'Admin\\' . self::$controller;
		if (method_exists(self::$controller, self::$action))
		{
			$dispatch = new self::$controller(self::$controller_name, self::$action);
			call_user_func_array([$dispatch, self::$action], self::$query_params);
		}
		else
		{
		    $dynamic = self::getExistDynamicController(self::$controller_name, self::$action_name);
		    if (!$dynamic)
		    {
                self::redirect('restricted');
            }


		}
	}

    /**
     * @param $controller mixed controller name
     * @param $action string function to be called
     * The url should be /posts/(category) or /posts/(category)/detail/(postId) or /(page) otherwise error
     * if url /posts/(category)/detail without postId it still error too
     * @return bool
     */
	public static function getExistDynamicController($controller, $action)
    {
        if ( strtolower($controller) != 'posts')
        {
            $dispatch = new DynamicPageController($controller, $action);
            if (strcmp(strtolower($action), 'index') != 0 ) return false; // if the url contain longer than 2 example /test/etc it will error
            call_user_func_array([$dispatch, 'indexAction'], [strtolower($controller)]);
        }
        else
        {
            $dispatch = new DynamicPostController($controller,$action);
            $category = $action; // store category to access query
            $action = 'indexAction'; // override data to access function in dynamic post controller
            if (isset(self::$query_params[0]) && !empty(self::$query_params[0]))
            {
                if(strcmp(strtolower(self::$query_params[0]), 'detail') == 0) $action = 'detailAction'; // to call detail post page
                else return false; // it will error if after /posts/(category)/.. is difference from 'detail'
            }
            if (count(self::$query_params) > 2) return false; // prevent url take longer than 2 after type category ex: /posts/testcategory/detail/01/etc it will error
            elseif(
                (count(self::$query_params) == 1 && strcmp(strtolower(self::$query_params[0]), 'detail') == 0)
                OR
                count(self::$query_params) == 2 && (empty(self::$query_params[1]) OR !intval(self::$query_params[1])) // postId must be int or not empty
            ) return false; // prevent url /posts/(category)/detail without id
            $param = [];
            if (isset(self::$query_params[1])) {
                $param['id'] = self::$query_params[1];
                $param['category'] = $category;
            }
            self::$query_params = null;
            call_user_func_array([$dispatch, $action], $param);
        }
        return true;
    }

	public static function redirect($location)
	{
		if (!headers_sent())
		{
			header('Location:' . PROJECT_PATH . $location);
		}
		else
		{
			echo '<script>window.location.href="'.PROJECT_PATH.$location.'"</script>';
			echo '<noscript><meta http-equiv="refresh" content="0;url=\''.PROJECT_PATH.$location.'\'"/></noscript>';
		}
	}

	public static function hasAccess($controller, $action = 'index')
	{
	    /*
		$acl_file = file_get_contents(ROOT . DS .'app'. DS .'config'. DS .'acl.json');
		$acl_json = json_decode($acl_file, true);
		$current_user_acl = ["Guest"];

		$grant_access = false;

		if (Session::exist(CURRENT_USER_SESSION_NAME)) 
		{
			$current_user_acl[] = "LoggedIn";
			foreach (FH::currentUser()->acls() as $value) {
				$current_user_acl[] = $value;
			}
		}

		// check grant access
		foreach ($current_user_acl as $level) {
			if (array_key_exists($level, $acl_json) && array_key_exists($controller, $acl_json[$level])) 
			{
				if (in_array($action, $acl_json[$level][$controller]) || in_array("*", $acl_json[$level][$controller])) {
					$grant_access = true;
					break;
				}
			}
		}

		foreach ($current_user_acl as $level) {
			$denied = $acl_json[$level]['Denied'];
			if (!empty($denied) && array_key_exists($controller, $denied) && in_array($action, $denied[$controller])) {
				$grant_access = false;
				break;
			}
		}
		return $grant_access;
	    */
        $is_loggedIn = Session::get('is_loggedIn');
        if (!$is_loggedIn)
        {
            Session::delete('UserRole');
            return false;
        }
        $user = FH::currentUser();
        switch ($user->user_type)
        {
            case SUPER_ADMIN_TYPE :
                // super admin nothing to check allow all
                return true;
                break;
            case ADMIN_TYPE :
                // admin
                $role = Session::get('UserRole') ?? $user->saveRole();
                 return self::getAuthorizeController($controller, $action, $role);
                break;
                // user and anonymous user aren't authorize
            default: return false;
        }
    }

    public static function getAuthorizeController($controller , $action, $role)
    {
        switch ($controller)
        {
            /*UserController*/
            case 'UserController':
                switch ($action) {
                    case 'indexAction': return $role['view_user'] == 1;
                    case 'disableAction': return $role['disable_user'] == 1;
                    case 'addAction': return $role['create_user'] == 1;
//                    case 'updateAction': return $role[''] == 1;
                    case 'editAction': return $role['update_user'] == 1;
                    case 'profileAction': return $role['view_profile'] == 1;
                }
                break;

            /*PostController*/
            case 'PostController':
                switch ($action)
                {
                    case 'indexAction': return in_array(1, [$role['view_all_post'], $role['edit_post'], $role['delete_post'], $role['approve_post'], $role['waiting_post'], $role['denied_post']]);
                    case 'addAction': return $role['create_post'] == 1;
                    case 'updateAction': return $role['edit_post'] == 1;
                    case 'deleteAction': return $role['delete_post'] == 1;
                    case 'approveAction': return $role['approve_post'] == 1;
                    case 'waitingAction': return $role['waiting_post'] == 1;
                    case 'deniedAction': return $role['denied_post'] == 1;
                }
                break;
            /*Category*/
            case 'CategoryController':
                switch ($action)
                {
                    case 'indexAction': return in_array(1, [$role['view_all_category'], $role['edit_category'], $role['delete_category'], $role['create_category']]);
                    case 'saveAction': return $role['create_category'] == 1;
                    case 'updateAction': return $role['edit_category'] == 1;
                    case 'deleteAction': return $role['delete_category'] == 1;
                }
                break;
            default: return true;
        }
        return true;
    }

	public static function getMenu($menu)
	{
		$menu_arr = [];
		$menu_file = file_get_contents(ROOT . DS .'app'. DS .'config'. DS . $menu .'.json');
		$menu_json_acl = json_decode($menu_file, true);

		foreach ($menu_json_acl as $key => $value) 
		{
			if (is_array($value)) 
			{
				$sub = [];
				foreach ($value as $k => $v) 
				{
					if ($k == 'separator' && !empty($sub))
					{
						$sub[$k] = '';
						continue;
					}
					elseif ($link = self::getLink($v))
					{
						$sub[$k] = $link;
					}
				}
				if (!empty($sub)) {
					$menu_arr[$key] = $sub;
				}
			}
			elseif ($link = self::getLink($value)) 
			{
				$menu_arr[$key] = $link;
			}
		}
		return $menu_arr;
	}

	private static function getLink($link)
	{
		if (preg_match('/https?:\/\//', $link) == 1)
		{
			return $link;
		}
		else
		{
			$arr = explode('/', $link);
			$controller = ucwords($arr[0]);
			$action = (isset($arr[1])) ? $arr[1] : '';
			if (self::hasAccess($controller, $action)) {
				return PROJECT_PATH . $link;
			}
			return false;
		}
	}

	public static function back(){
		echo Dom::script([], 'history.back();');
	}

	public static function forward(){
		echo Dom::script([], 'history.forward();');
	}
}