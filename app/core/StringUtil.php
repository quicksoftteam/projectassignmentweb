<?php


namespace app\core;


class StringUtil
{
    public static function len($a)
    {
        return mb_strlen($a, 'UTF-8');
    }

    public static function charAt($a, $i)
    {
        return self::substr($a, $i, 1);
    }

    public static function substr($a, $x, $y = null)
    {
        if ($y === NULL) {
            $y = self::len($a);
        }
        return mb_substr($a, $x, $y, 'UTF-8');
    }

}