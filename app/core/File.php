<?php
namespace app\core;
use App\config\config;

/**
 * File class contain all functions to help upload file image
 * @Author sinath
 * @Date: 23/02/2020 11:15 pm
 */

class File
{

    private $extention = ["jpg","jpeg","png","gif"];

    function getExtension($file){
        $extension = pathinfo($_FILES[$file]['name']);
        return $extension['extension'];
    }

    public function checkExtension($extension_file){
        if(in_array($this->extension,$extension_file)){
            return true;
        } 
        return false;
    }
    
    public static function uploadImage($file)
    {
	    if(!($_FILES[$file]['error']))
	    {
            $new_name = time() . '.png';
            $destination = ROOT.DS.PATH_IMAGE_USERS_UPLOAD . $new_name;
            move_uploaded_file($_FILES[$file]['tmp_name'], $destination);
            return $new_name;
	    }
	    return null;
    }

    public static function deleteUploadImage($name)
    {
        if (file_exists(ROOT.DS.PATH_IMAGE_USERS_UPLOAD .$name) && !empty($name))
        {
            return unlink(ROOT.DS.PATH_IMAGE_USERS_UPLOAD .$name);
        }
        return false;
    }
}