<?php

namespace App\Core;
use App\Core\H;
class  View
{
	protected $_siteTitle = SITE_TITLE;
	protected $_head;
	protected $_body;
	protected $_script;
	protected $_outputBuffer;
	protected $_layout = DEFAULT_LAYOUT;

	public function render($viewName)
	{
		$view_array = explode('/', $viewName);
		$view_string = implode(DS, $view_array);
		if (file_exists(VIEW_PATH . DS .$view_string.'.php'))
		{
			include (VIEW_PATH . DS .$view_string.'.php');

			if (file_exists(VIEW_PATH. DS .'layouts'. DS . $this->_layout .'.php'))
			{
				include (VIEW_PATH. DS .'layouts'. DS . $this->_layout .'.php');
			}
			else
			{
				include (VIEW_PATH . DS . implode( DS ,explode('/',$this->_layout)) .'.php');
			}
		}
		else
		{
			die("can not find the view $view_string");
		}
	}

	public function content($type)
	{
		if ($type == 'head')
		{
			return $this->_head;
		}
		elseif ($type == 'body')
		{
			return $this->setCsrfInput($this->_body);
		}
		elseif ($type == 'script')
		{
			return $this->_script;
		}
		return false;
	}
    public function setCsrfInput($contentPage)
    {
        $contentPage = preg_replace("/(<[fF][oO][rR][mM][^>]*>)/", "\\1".FH::tokenInput(), $contentPage);
        return $contentPage;
    }
    
	public function start($type)
	{
		$this->_outputBuffer = $type;
		ob_start();
	}

	public function end()
	{
		if ($this->_outputBuffer == 'head')
		{
			$this->_head = ob_get_clean();// mb_convert_encoding( ob_get_clean(), 'UTF-8', 'UTF-8' );
		}
		elseif ($this->_outputBuffer == 'body')
		{
			$this->_body =ob_get_clean();// mb_convert_encoding( ob_get_clean(), 'UTF-8', 'UTF-8' );
		}
		elseif ($this->_outputBuffer == 'script')
		{
			$this->_script = ob_get_clean();//mb_convert_encoding( ob_get_clean(), 'UTF-8', 'UTF-8' );
		}
		else
		{
			H::dd('you must run method start first.');
		}
	}

	public function setSiteTitle($title)
	{
		$this->_siteTitle = $title;
	}

	public function setLayout($path)
	{
		$this->_layout = $path;
	}

	public function insert($view_path)
	{
		include ROOT . DS .'app'. DS .'view'. DS . $view_path .'.php';
	}

	public function fragement($group, $fragement)
	{
		include ROOT . DS .'app'. DS .'views'. DS . $group . DS . $fragement .'.php';
	}
}