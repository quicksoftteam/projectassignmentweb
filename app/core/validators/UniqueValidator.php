<?php

namespace App\Core\Validators;
use App\Core\Validators\BaseValidator;

class UniqueValidator extends BaseValidator
{

	public function runValidator()
	{
		$field = is_array($this->field) ? $this->field[0] : $this->field;
		$value = $this->_model->{$field};
		$condition = [];
		$bind = [];
		if (!empty($this->_model->id))
		{
			$condition[] = "id != ?";
			$bind[] = $this->_model->id;
		}

		if (is_array($this->field))
		{
			array_unshift($this->field);
			foreach ($this->field as $field)
			{
				$condition[] = "{$field} = ?";
				$bind[] = $this->_model->{$field};
			}
		}

		$query_params = ['conditions' => $condition, 'bind' => $bind];
		$other = $this->_model->findFirst($query_params);
		return !$other;
	}
}