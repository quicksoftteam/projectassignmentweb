<?php

namespace App\Core\Validators;
use App\Core\Validators\BaseValidator;

class MinValidator extends BaseValidator
{

	public function runValidator()
	{
		$value = $this->_model->{$this->field};
		return (strlen($value) >= $this->rule);
	}
}