<?php

namespace App\Core\Validators;
use App\Core\Validators\BaseValidator;

class MatchesValidator extends BaseValidator
{

	public function runValidator()
	{
		$value = $this->_model->{$this->field};
		return $value == $this->rule;
	}
}