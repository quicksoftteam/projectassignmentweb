<?php

namespace App\Core\Validators;
use App\Core\Validators\BaseValidator;

class NumericValidator extends BaseValidator
{

	public function runValidator()
	{
		$value = $this->_model->{$this->field};
		$pass = true;
		if (!empty($value))
		{
			$pass = is_numeric($value);
		}
		return $pass;
	}
}