<?php

namespace App\Core\Validators;

use App\Core\H;
use Exception;

abstract class BaseValidator
{
	public $success = true;
	public $msg = '';
	public $field;
	public $rule;
	protected $_model;

	public function __construct($model, $params)
	{
		$this->_model= $model;
		if (!array_key_exists('field', $params)) 
		{
			throw new Exception("You must add a field to the pass params array.");
		}
		else
		{
			$this->field = is_array($params['field']) ? $params['field'][0] : $params['field'];
		}

		if (!property_exists($model, $this->field)) 
		{
			throw new Exception("The field must exist in the model");
		}

		if (!array_key_exists('msg', $params))
		{
			throw new Exception("You must add a msg to the pass params array");
		}
		else
		{
			$this->msg = $params['msg'];
		}

		if (array_key_exists('rule', $params)) 
		{
			$this->rule = $params['rule'];
		}

		try {
			$this->success = $this->runValidator();
		} catch (Exception $e) {
			H::dd("Validation Exception on ".get_class()." : ".$e->getMessage()."<br/>", false);
		}
	}
	public abstract function runValidator();
}