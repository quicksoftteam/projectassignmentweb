<?php

namespace App\Core\Validators;
use App\Core\Validators\BaseValidator;

class EmailValidator extends BaseValidator
{

	public function runValidator()
	{
		$value = $this->_model->{$this->field};
		$pass = true;
		if (!empty($value))
		{
			$pass = filter_var($value, FILTER_VALIDATE_EMAIL);
		}
		return $pass;
	}
}