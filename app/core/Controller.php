<?php
namespace App\Core;
use App\Core\Application;
use App\Models\User;
class Controller extends Application
{
	protected $_controller, $_action;
	public $view;
	public $request;
	public function __construct($controller, $action)
	{
		parent::__construct();
		$this->_controller = $controller;
		$this->_action = $action;
		$this->request = new Input();
		$this->view = new View();
		
		$currentUser = FH::currentUser()?? new User();
		$this->view->currentUser = $currentUser;
	
	}
	
	protected function loadModel($model)
	{
		$model_path = "App\Models\\".$model;
        if (class_exists($model_path))
        {
            $this->{$model.'Model'} = new $model_path();
        }
	}

	public function responseJson($statusCode, $success, $data, $message = '')
    {
        $response = ['success' => $success, 'data' => $data ];
        $response = strlen(trim($message)) > 0 ? array_merge($response, ['message'=>$message]) : $response;
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        http_response_code($statusCode);
        echo json_encode($response);
        exit;
    }

}