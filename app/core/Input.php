<?php

namespace App\Core;
use App\Core\FH;
use App\Core\Router;
class Input
{
	public function isPost()
	{
		return $this->getRequestMethod() === 'POST';
	}

	public function isPut()
	{
		return $this->getRequestMethod() === 'PUT';
	}

	public function isGet()
	{
		return $this->getRequestMethod() === 'GET';
	}

    public function isDelete()
    {
        return $this->getRequestMethod() === 'DELETE';
    }

	public function getRequestMethod()
	{
		return strtoupper($_SERVER['REQUEST_METHOD']);
	}
	public static function sanitize($value)
	{
		return htmlentities($value, ENT_QUOTES, 'UTF-8');
	}

	public function get($input = false)
	{
		if (!$input)
		{
			$data = [];
			foreach ($_REQUEST as $field => $value)
			{
				$data[$field] = Input::sanitize($value);
			}
			return $data;
		}
		return isset($_REQUEST[$input])? Input::sanitize($_REQUEST[$input]) : null;
	}

	public function csrfCheck()
	{
		if (!FH::checkToken($this->get('csrf_token'))) Router::redirect('restricted/badToken');
		return true;
	}
}