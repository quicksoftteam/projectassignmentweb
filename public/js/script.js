function _ajax_request(url, data, callback, method) {
    return $.ajax({
        url: project_path + url,
        type: method,
        data: data,
        success: callback
    });
}
$.extend({
    put: function(url, data, callback) {
        return _ajax_request(url, data, callback, 'PUT');
    }
});
$.extend({
    delete: function(url, data, callback) {
        return _ajax_request(url, data, callback, 'DELETE');
    }
});
$.extend({
    post: function(url, data, callback) {
        return _ajax_request(url, data, callback, 'POST');
    }
});
$.extend({
    get: function(url, data, callback) {
        return _ajax_request(url, data, callback, 'GET');
    }
});

function spinner(visible){
    let main = $('<div/>');
    let spinner = $("<div/>",{ class : 'd-flex flex-fill', role: 'status'});
    // let type = ['text-primary', 'text-secondary', 'text-success', 'text-danger', 'text-warning', 'text-info','text-light'];
    let type = ['text-danger', 'text-warning', 'text-success', 'text-info'];
    type.forEach( function (t) {
        $(spinner).append($("<div/>", { class: 'spinner-grow ' + t, role : 'status' })
            .append($('<span/>', { class : 'sr-only', text : 'Loading...' })));
    });
    $(main).append(spinner);
    return $.LoadingOverlay(visible, {
        image       : "",
        custom      : $(main).html()
    });
}

$.extend({
    showSpinner: function() {
        return spinner('show');
    }
});

$.extend({
    hideSpinner: function() {
        return spinner('hide');
    }
});

status = {
    internalError : 500,
    errorNotFound : 404,
    errorNotAuthorize : 401,
    success : 200
};

(function() {
    'use strict';
    window.addEventListener('load', function() {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

$('a').on('click', function (e) {
    //e.preventDefault();
});