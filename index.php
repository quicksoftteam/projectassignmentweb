<?php

use App\Core\Session;
use App\Core\Cookie;
use App\Core\H;
use App\Core\Router;
use App\Models\User;

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));
define('APP_PATH', ROOT . DS .'app');
define('CONFIG_PATH', APP_PATH. DS .'config');
define('CORE_PATH', APP_PATH. DS.'core');
define('CONTROLLER_PATH', APP_PATH. DS.'controllers');
define('VIEW_PATH', APP_PATH. DS.'views');
define('MODEL_PATH', APP_PATH. DS .'models');
define('CUSTOM_VALIDATION_PATH', APP_PATH. DS .'custom_validators');
define('VALIDATOR_PATH', CORE_PATH. DS .'validators');
define('ADMIN_CONTROLLER_PATH', CONTROLLER_PATH. DS . 'admin');
define('APACHE_SERVER', 'Apache');
define('NGINX_SERVER', 'Nginx');
define('UNKNOWN_SERVER', 'NotSupportedServer');

require_once(CONFIG_PATH. DS .'config.php');
require_once(ROOT . DS .'app'. DS .'lib'. DS .'helpers'. DS .'functions.php');

function autoload($className)
{
	$class_arr = explode('\\', $className);
	$class = array_pop($class_arr);
	$sub_path = strtolower(implode(DS, $class_arr));
	$path = ROOT . DS . $sub_path . DS . $class .'.php';
	if (file_exists($path)){
		require_once $path;
	}
}
spl_autoload_register('autoload');
session_start();

if(!H::isProduction())
	define('PROJECT_PATH', '/projectassignmentweb/');
else
	define('PROJECT_PATH', '/');
$engine = H::getEngineName();

if ($engine == APACHE_SERVER)
{
    $url = isset($_SERVER['PATH_INFO']) ? explode('/', ltrim($_SERVER['PATH_INFO'], '/')) : [];
}
elseif($engine == NGINX_SERVER)
{
	$url = isset($_SERVER['REQUEST_URI']) ? explode('/', ltrim($_SERVER['REQUEST_URI'], '/')) : [];
}


if (!Session::exist(CURRENT_USER_SESSION_NAME) && Cookie::exist(REMEMBER_ME_COOKIE_NAME))
{
	User::loginUserFromCookie();
}
Router::route($url);
