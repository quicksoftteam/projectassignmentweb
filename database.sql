/*
 Navicat Premium Data Transfer

 Source Server         : Projects
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : 10.8.0.1:3306
 Source Schema         : projectweb

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 19/01/2020 17:31:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'science');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '0 if menu is root level or menuid if this is child on any menu',
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT '0 for disabled menu or 1 for enabled menu',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'Home', 0, '#home', '1');
INSERT INTO `menu` VALUES (2, 'Web development', 0, '#web-dev', '1');
INSERT INTO `menu` VALUES (3, 'WordPress Development', 0, '#wp-dev', '1');
INSERT INTO `menu` VALUES (4, 'About w3school.info', 5, '#w3school-info', '1');
INSERT INTO `menu` VALUES (5, 'AWS ADMIN', 2, '#', '1');
INSERT INTO `menu` VALUES (6, 'PHP', 2, '#', '1');
INSERT INTO `menu` VALUES (7, 'Javascript', 2, '#', '1');
INSERT INTO `menu` VALUES (8, 'Elastic Ip', 5, '#electic-ip', '1');
INSERT INTO `menu` VALUES (9, 'Load balacing', 5, '#load-balancing', '1');
INSERT INTO `menu` VALUES (10, 'Cluster Indexes', 5, '#cluster-indexes', '1');
INSERT INTO `menu` VALUES (11, 'Rds Db setup', 5, '#rds-db', '1');
INSERT INTO `menu` VALUES (12, 'Framework Development', 6, '#', '1');
INSERT INTO `menu` VALUES (13, 'Ecommerce Development', 6, '#', '1');
INSERT INTO `menu` VALUES (14, 'Cms Development', 6, '#', '1');
INSERT INTO `menu` VALUES (21, 'News & Media', 6, '#', '1');
INSERT INTO `menu` VALUES (22, 'Codeigniter', 12, '#codeigniter', '1');
INSERT INTO `menu` VALUES (23, 'Cake', 12, '#cake-dev', '1');
INSERT INTO `menu` VALUES (24, 'Opencart', 13, '#opencart', '1');
INSERT INTO `menu` VALUES (25, 'Magento', 13, '#magento', '1');
INSERT INTO `menu` VALUES (26, 'Wordpress', 14, '#wordpress-dev', '1');
INSERT INTO `menu` VALUES (27, 'Joomla', 14, '#joomla-dev', '1');
INSERT INTO `menu` VALUES (28, 'Drupal', 14, '#drupal-dev', '1');
INSERT INTO `menu` VALUES (29, 'Ajax', 7, '#ajax-dev', '1');
INSERT INTO `menu` VALUES (30, 'Jquery', 7, '#jquery-dev', '1');
INSERT INTO `menu` VALUES (31, 'Themes', 3, '#theme-dev', '1');
INSERT INTO `menu` VALUES (32, 'Plugins', 3, '#plugin-dev', '1');
INSERT INTO `menu` VALUES (33, 'Custom Post Types', 3, '#', '1');
INSERT INTO `menu` VALUES (34, 'Options', 3, '#wp-options', '1');
INSERT INTO `menu` VALUES (35, 'Testimonials', 33, '#testimonial-dev', '1');
INSERT INTO `menu` VALUES (36, 'Portfolios', 33, '#portfolio-dev', '1');

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (1, 'hahaha', '<h1>Hello Hahaha Page</p>', '2020-01-18 17:34:55');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `date` datetime(0) NOT NULL,
  `id` int(11) NOT NULL,
  `image_feature` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('Hello post page', '<p>ក្នុ​ង​​រ​ដូវ​បុ​ណ្យ​ចូ​ល​ឆ្នាំ​ខ្មែរ​ ​ប្រ​ពៃ​ណី​ជា​តិ ​ក​ន្ល​ងទៅ​នេះ​​\r\n ​រម​ណី​យ​ដ្ឋា​នភ្នំ​បូ​ក​គោ​ ​ដែ​ល​មា​​នទី​តាំ​​ង​ \r\nស្ថិ​តនៅ​ក្នុ​ង​ស្រុ​កទឹ​​ក​ឈូ ​ខេ​ត្ត​កំ​ពត​ ​ស​ម្បូ​រ​ទេ​ស​ភា​ព​ \r\n​ស្រ​ស់​ត្រ​កាល​​ហ៊ុ​ម​ព័​ទ្ធ​ដោ​យ​រុ​ក្ខជា​តិ​ខៀ​វ​ស្រងា​ត់​ \r\nនិ​ងដុំ​ព​ព​កក្រាស់​ឃ្មឹ​ក​​ដូ​ចអ័​ព្ទ​​ \r\n​ទ​ទួ​ល​បា​ន​ចំណា​ប់​អា​​រម្មណ៍​យ៉ាង​ខ្លាំ​ង​ពី​សំ​ណា​ក់​ភ្ញៀវ​ទេ​ស​ច​រ​\r\n ជា​តិ និងអ​ន្ត​រ​ជា​តិ​ ​ក្នុង​កា​រ​ទៅ​ទ​ស្ស​នា​ក​ម្សា​ន្តស​ប្បា​យ \r\nប៉ុន្តែ ជាអកុសល ដោយសាររមណីយដ្ឋាននេះ ខ្វះជម្រក និងកន្លែងសម្រាប់បន្ទោបង់ \r\nធ្វើឱ្យភ្ញៀវជាច្រើន ដែល ទៅទស្សនាមានការត្អូញត្អែរ \r\nនិងថ្នាំងថ្នាក់យ៉ាងខ្លាំង...។ លោកសយ ស៊ីណុល ប្រធានមន្ទីរទេសចរណ៍ ខេត្តកំពត\r\n បានឱ្យដឹងថា ក្នុងរយៈពេល៥ថ្ងៃ ចាប់ពីថ្ងៃទី ១៣ ដល់១៧ ខែមេសា ឆ្នាំ២០១១ \r\nមានភ្ញៀវទេស- ចរមកពីតាមបណ្តាខេត្តនានា នៅក្នុងប្រទេស បាន ឡើងទៅលេងកម្សាន្ត \r\nនៅរមណីយដ្ឋានភ្នំបូកគោ សរុបចំនួន៤៧.៤៦៩នាក់ ក្នុងនោះមានចំនួន៣៧៧ នាក់ \r\nជាជនបរទេស។ លោកសយ ស៊ីណុល បានមានប្រសាសន៍ថា ភ្ញៀវដែលទៅទស្សនារមណីយដ្ឋាននេះ \r\nមួយភាគធំច្រើន ឡើងទៅលើខ្នងភ្នំ ដើម្បីមើលទេសភាព ស្រូបយក \r\nខ្យល់អាកាសដែលមានធាតុត្រជាក់ មើលពពកវិល \r\nនិងមើលសំណង់អគារចាស់ៗដែលបន្សល់ទុកមកពីជំនាន់មុន។ លោកសយ ស៊ីណុល បានបន្តទៀតថា\r\n នៅថ្ងៃទី ១៣ និង១៤ដែលជាថ្ងៃបើកដំបូង រមណីយដ្ឋាន មិន \r\nសូវមានអ្នកទេសចរឡើងទៅលេងកម្សាន្តទេ ប៉ុន្តែ នៅថ្ងៃទី១៥-១៦ និង១៧ \r\nដែលជាថ្ងៃចុងក្រោយ នៃ ពិធីបុណ្យចូលឆ្នាំខ្មែរ មានភ្ញៀវឡើងទៅលេងច្រើន \r\nលើសធម្មតា ដោយក្នុងមួយថ្ងៃ មានរថយន្តឡើង ប្រមាណជិត២.០០០គ្រឿង។ លោកសយ ស៊ីណុល\r\n បានគូសបញ្ជាក់ថា “យើង បាត់ម្ចាស់ការក្នុងការគ្រប់គ្រងការងារសណ្តាប់ធ្នាប់ \r\nនៅលើកំពូលភ្នំនេះ ដោយសារចំនួនភ្ញៀវ ដែលឡើងទៅ លេងកម្សាន្ត \r\nមានច្រើនជាងការរំពឹងទុករបស់យើង”។ យោងតាមស្ថិតិ \r\nដែលទទួលបានពីមន្ទីរទេស-ចរណ៍ខេត្តកំពត បានឱ្យដឹងថា កាលពីរដូវបុណ្យចូល \r\nឆ្នាំខ្មែរ ឆ្នាំមុន រមណីយដ្ឋានព្រះមុនីវង្សបូកគោ ដែល \r\nស្ថិតនៅចម្ងាយប្រមាណ៨គីឡូម៉ែត្រ ពីទីរួមខេត្តកំពត \r\nហើយមានផ្លូវឡើងដល់ទៅប្រមាណ៣២គីឡូម៉ែត្រ ចាប់ពីជើងភ្នំ \r\nទៅដល់ខ្នងភ្នំខាងលើនោះ \r\nទទួលបានភ្ញៀវ-ទេសចរទៅលេងកម្សាន្តសរុបចំនួនតែ២១.៨៩២នាក់ ក្នុងនោះ </p>', 1, '2020-01-19 12:38:03', 1, 'sample.jpg', 5, 1);
INSERT INTO `posts` VALUES ('here update', 'រមណីដ្ឋានភ្នំបូកគោនេះស្ថិត \r\n​នៅ​ចម្ងាយ​៤២​គីឡូម៉ែត្រ​ពី​ទី​រួម​ខេត្ត​កំពត​ហើយ​ឧទ្យាន​ជាតិ​ភ្នំ​បានរកឃើញដោយជនជាតិបារាំងមួយឈ្មោះថា\r\n រូលួស ហើយត្រូវបានរៀបចំឡើងនៅថ្ងៃទី13-4-1922 \r\nក្នុងរាជ្ជកាលព្រះបាទស៊ីសុវត្តិ។ កាលសម័យរាស្រ្តនិយម គេបានកសាងសំណង់សាធារណៈ\r\n ជាច្រើនប្តែក្លាយជាបូរីមួយ។ ទទឹមនឹងនោះមានកន្លែងទឹកជ្រោះមួយ \r\nនៅតំបន់ទឹកធ្លាក់ ៣ ថ្នាក់ ពពកវិល \" ដែលមាន ចំងាយ ៧គ.ម \r\nពីទីនោះ។ភ្នំ​បូក​គោ​ស្ថិត​នៅ​រយៈ​កម្ពស់​១០៧៥​មែ​ត្រ​មាន​ខ្យល់អាកាស​ល្អ​បរិសុទ្ធ​\r\n ទេសភាព​ធម្មជាតិ​ដ៏​ស្រស់​ត្រកាល​និង​សែនមនោរម្យ​ក្រៃលែង​ជាមួយ​នឹង​ដើមឈើ​\r\n ធំៗ \r\nនិង​ផ្ទាំង​ថ្ម​ដែល​មាន​រាង​ដូច​ជា​សត្វ​។​នៅ​ក្នុង​សម័យ​សង្គមរាស្ត្រនិយម​\r\n ​មាន​សំណង់​អាគារ​សាធារណៈ​ជា​ច្រើន​ប្រៀប​ដូច​ជាទី​ក្រុង​មួយ​ដែរ ។ \r\nនៅ​ចម្ងាយ​៧គីទ្បូមែត្រ​ពី​ភ្នំបូកគោ​មាន​កន្លែង​សម្រាប់ងូតទឹកកំសាន្ត \r\nដែល​ស្ថិត​នៅ​កន្លែង​ទឹកធ្លាក់។ ទីនោះ​មានឈ្មោះ​ថា​ពពក​វិល​។ \r\nរីឯ​នៅ​​លើ​កំពូលភ្នំ​វិញ​គេ​អាច​មើល​ឃើញ​ទិដ្ឋភាព​ដ៏​ស្រស់​ស្អាត​នៃ​ \r\nពីលើកំពូលភ្នំបូកគោគេអាចមើលឃើញទិដ្ឋភាពខ្លះៗ នៃទីក្រុងកំពត កែបនិង \r\nខេត្តព្រះសីហនុ ព្រមទាំងផ្ទៃសមុទ្រខៀវស្រង៉ាត់ធំល្វឹងល្វើយជាប់ជើងមេឃ។\r\n                ', 1, '2020-01-19 12:38:03', 2, 'sample.jpg', 4, 1);

-- ----------------------------
-- Table structure for replies
-- ----------------------------
DROP TABLE IF EXISTS `replies`;
CREATE TABLE `replies`  (
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `reply` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(11) NOT NULL,
  `edit_menu` tinyint(1) NOT NULL,
  `create_user` tinyint(1) NOT NULL,
  `update_user` tinyint(1) NOT NULL,
  `approve_post` tinyint(1) NOT NULL,
  `disable_post` tinyint(1) NOT NULL,
  `edit_post` tinyint(1) NOT NULL,
  `create_post` tinyint(1) NOT NULL,
  `disable_user` tinyint(1) NOT NULL,
  `view_profile` tinyint(1) NOT NULL,
  `update_user_role` tinyint(1) NOT NULL,
  `create_status` tinyint(1) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status`  (
  `id` int(11) NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_sessions
-- ----------------------------
DROP TABLE IF EXISTS `user_sessions`;
CREATE TABLE `user_sessions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sessions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_sessions
-- ----------------------------
INSERT INTO `user_sessions` VALUES (23, 1, '09f5a1c84f996a32ea0d72ef586a70bd', 'Mozilla (Windows NT 10.0; Win64; x64; rv:71.0) Gecko Firefox');

-- ----------------------------
-- Table structure for user_type
-- ----------------------------
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date_create` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'add new for implement with table users that has relationship ( It need the foreign key)' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_type
-- ----------------------------
INSERT INTO `user_type` VALUES (1, 'Admin', '2020-01-15');
INSERT INTO `user_type` VALUES (2, 'User', '2020-01-15');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `email` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `fname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `csrf_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `deleted` tinyint(4) NULL DEFAULT 0,
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `role_id` tinyint(4) NULL DEFAULT 0,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `user_type` int(11) NULL DEFAULT NULL,
  `photo` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin@example.com', '$2y$10$LUjlWQ4bRazkdhxxK4EeuOclk/lF0LCpXPj49ek3wNQbnS4SHanK.', '', '', '', 0, NULL, 0, '0000-00-00 00:00:00', 1, NULL);
INSERT INTO `users` VALUES (4, 'admin3', 'admin3@exmple.com', '$2y$10$hpGhgcM/RJPIYuhpGkrlC.qgpQvF0KoRl53gC9g7qAfnJ1MLyj5EC', '', '', '', 0, NULL, 0, '0000-00-00 00:00:00', 1, NULL);
INSERT INTO `users` VALUES (5, 'adminweb', 'admin1@example.com', '$2y$10$b3LdQqIRprVZCPo9dkuIg.1YFfBopYbHePMpIBIK47oteJ2uGVvkm', 'web', 'admin', '', 0, NULL, NULL, '0000-00-00 00:00:00', 2, NULL);
INSERT INTO `users` VALUES (6, 'root', 'admin4@example.com', '$2y$10$bV/OkMJucaOma8u3DPBu6e0IZm0DvsLbfrHijsbiGYdjidrDdm8P2', 'd', 'd', NULL, 0, NULL, NULL, '0000-00-00 00:00:00', 2, NULL);
INSERT INTO `users` VALUES (7, '111111', '', '', '', '', NULL, 0, NULL, NULL, '0000-00-00 00:00:00', 2, NULL);
INSERT INTO `users` VALUES (8, 'longchong', 'sokhailongchong@gmail.com', '$2y$10$Gz6kznq593aLjEFxlHF2LeD5xBLCnuBSQbDft39xtqB19RKwuYQ/y', 'long', 'sokhai', NULL, 0, NULL, NULL, NULL, 0, NULL);
INSERT INTO `users` VALUES (9, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL);
INSERT INTO `users` VALUES (10, 'test', 'test', NULL, NULL, NULL, 'IMfXlUUwOGc/FBbHnzZ2GeFF+JcWQ602P1M539xTAao=', 0, NULL, 0, NULL, 1, NULL);
INSERT INTO `users` VALUES (11, 'test', 'test', NULL, NULL, NULL, 'IMfXlUUwOGc/FBbHnzZ2GeFF+JcWQ602P1M539xTAao=', 0, NULL, 0, NULL, 1, NULL);
INSERT INTO `users` VALUES (12, 'hello', 'hellwwwwwwo', NULL, NULL, NULL, 'utRiJk0kH4O5rI/Ab7n2fPb7T6vFXckR0eXrJ/sTDpY=', 0, NULL, 0, NULL, 1, NULL);
INSERT INTO `users` VALUES (13, 'hello', 'hellwwwwwwo', NULL, NULL, NULL, 'utRiJk0kH4O5rI/Ab7n2fPb7T6vFXckR0eXrJ/sTDpY=', 0, NULL, 0, NULL, 1, NULL);
INSERT INTO `users` VALUES (14, '', '', NULL, NULL, NULL, 'gKMqAbNpibGVofKWAzO2SAybtzMTbrLVhXFPdPgPr4k=', 0, NULL, 0, NULL, 0, NULL);

-- ----------------------------
-- Table structure for views
-- ----------------------------
DROP TABLE IF EXISTS `views`;
CREATE TABLE `views`  (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `post_id` int(11) NOT NULL,
  `date` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
